grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;


import ast.*;
import lib.*;
}

@parser::members{
int stErrors=0;

private int nestingLevel = 0;
private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();

/*
 * Il layout degli AR globale, in cui troviamo
 * le dichiarazioni delle classi, delle variabili, delle funzioni
 * contiene Control Link 
 * 			Return Address
 * e, a offset, -2 la classe/funzioni/variabili
 */
private int globalOffset = -2;

/*
 * Serve per preservare le dichiarazioni interne ad una classe (campi e metodi)
 * per renderli accessibili anche in seguito alla dichiarazione della classe
 */
private HashMap<String, HashMap<String, STentry>> classTable = new HashMap<>();
}

@lexer::members {
int lexicalErrors=0;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
 
prog returns [Node ast]
	: {
	   /*
	    * Scope entry:
	    * - creo una nuova tabella
	    * - la aggiungo al fronte della lista
	    */	
	   HashMap<String,STentry> hm = new HashMap<String,STentry> ();
       symTable.add(hm);
       }          
	  ( e=exp 	
        {$ast = new ProgNode($e.ast);} 
      |  LET {
      	ArrayList<Node> classList = new ArrayList<>();
      	ArrayList<Node> declList = new ArrayList<>();
      }
      		( c=cllist { classList.addAll($c.classList);}
      			(d=declist {declList.addAll($d.astlist);})? 
		      | d=declist {declList.addAll($d.astlist);}
			) IN e=exp  {
				$ast = new ProgLetInNode(classList, declList, $e.ast);  
			}
	  ) 
	  {
	  	/*
	  	 * Scope exit:
	  	 * - Rimuovo la HashMap al nesting level corrente
	  	 */
	  	symTable.remove(nestingLevel);
	  }
      SEMIC EOF ;

cllist returns [ArrayList<Node> classList]:
	{
		$classList = new ArrayList<>();
	}
	
	( CLASS idClass=ID
		{

		    HashMap<String, STentry> virtualTable = new HashMap<>();
			HashMap<String, STentry> superVirtualTable = new HashMap<>();          	


			ClassTypeNode classTypeNode = new ClassTypeNode();  
			ClassNode classNode = new ClassNode($idClass.text, classTypeNode);	
			Set<String> classDeclarations = new HashSet<>();
			
			int methodOffset = 0;
			int fieldOffset = -1;	
		}
		
		(EXTENDS idSuperClass=ID
			{
				/*
            	 * Cerchiamo la STentry di ID della superClass tramite
            	 * la discesa di livelli della symbol table 
            	 */
				STentry entry = symTable.get(0).get($idSuperClass.text);
				
				if (entry == null) {
					System.out.println("Extended Class id "+$idSuperClass.text+" at line "+$idSuperClass.line+" already declared");
	      			stErrors++;
				} else {
					classNode.setSuperEntry(entry);
					FOOLlib.setSuperType($idClass.text, $idSuperClass.text);
				}
				
				try {
					classTypeNode = (ClassTypeNode) entry.getType().cloneNode();
	
					classNode.setFields(classTypeNode.getFields());
					classNode.setMethods(classTypeNode.getMethods());
					classNode.setSuperEntry(entry);
					classNode.setExtendId($idSuperClass.text);
											
					superVirtualTable = classTable.get($idSuperClass.text);
	 				Set<String> keys = superVirtualTable.keySet(); 
	 
	 				/*
	 				 * Eredita campi e metodi della virtual table del padre
	 				 */
	 				for (String k : keys) {
						virtualTable.put(k, superVirtualTable.get(k).cloneNode());
	 				}				
				} catch (CloneException e) {
					e.printStackTrace();
				}
			
				/*
				 * Nel caso in cui si eredita gli offset vengono aggiornati in base alla
				 * lunghezza di fields e methods di ClassTypeNode in STentry.
				 * -lunghezza-1 per i campi e lunghezza per i metodi
				 */
				methodOffset = classTypeNode.getMethods().size();
				fieldOffset = -classTypeNode.getFields().size();
			}
			
		// Fine dell'EXTENDS
		)? 
		
		{								
	       	STentry entry = STentry.getEntry(nestingLevel, globalOffset); 
	       	entry.setType(classTypeNode);
	       	
			globalOffset = globalOffset - 1;
	       	
			HashMap<String,STentry> hm = symTable.get(nestingLevel);
	        
	        if ( hm.put($idClass.text,entry) != null  ) {
	      		System.out.println("Class id "+$idClass.text+" at line "+$idClass.line+" already declared");
	      		stErrors++;
	     	} 
	        
	        classTable.put($idClass.text, virtualTable); 
	        
		   /*
	    	* Scope entry:
	    	* - aggiungo la virtual table al fronte della lista
	    	* - incremento il nesting level per entrare in un nuovo scope
	    	*/
	    	symTable.add(virtualTable);	                         	
	
	        nestingLevel = nestingLevel + 1;
		}
		
		LPAR (idParameter=ID COLON t=type
			
			{
			   /* Ottimizzazione
	 	        * - Non è necessario controllare la ridefinizione (erronea)
	  	        *    del campo perchè è il primo.
		        * - Non sono ancora stati dichiarati campi
		        *   (e quindi non ce ne sono con lo stesso nome).
		        */
				FieldNode field = new FieldNode($idParameter.text, $t.ast);
				
				STentry fieldEntry = null; 
				
				int entryOffset;

				/*
				 * Se nome di campo è già presente non lo considero errore: è overriding
				 * sostituisco nuova STentry alla vecchia preservando
				 * l'offset che era nella vecchia STentry
				 */ 
		        if (virtualTable.containsKey($idParameter.text)) {            	
		        	STentry superEntry = virtualTable.get($idParameter.text);
					int superOffset = superEntry.getOffset();
		        	
		        	/*
		        	 * Non consento l'overriding di un campo con un metodo 
		        	 */ 
		        	if (superEntry.isMethod()) {
						System.out.println("Field id "+ $idParameter.text+" at line "+$idParameter.line+" is overriding a method");
						stErrors++;	    				        		
		        	} else {	            	

		            	fieldEntry = STentry.getEntry(nestingLevel, superOffset);
		            	
		            	/* Ottimizzazione */
		            	field.setOffset(superOffset);		            		  
		            	
		            	/*
		            	 * Per i campi aggiorno array allFields settando
		            	 * la posizione -offset-1 in base all'offset dell'STentry
		            	 * (Nel nostro layout l'offset del primo campo è -1)
		            	 */
						classTypeNode.addOverrideField((-superOffset-1), field);
						classNode.addOverrideField((-superOffset-1), field);
					}
				} else {
		     		classTypeNode.addField((-fieldOffset-1), field);
					classNode.addField((-fieldOffset-1), field);
				
					field.setOffset(fieldOffset);
		     		fieldEntry = STentry.getEntry(nestingLevel, fieldOffset--);
		     	}
		     	
		     	fieldEntry.setType($t.ast);
				virtualTable.put($idParameter.text, fieldEntry);							
			}
			
			(COMMA otherIdParameter=ID COLON ot=type
				
				{
					   /* Ottimizzazione:
           				* Controlliamo che i nomi dei campi non siano già stati assegnati.
           				*/
						if (classDeclarations.contains($otherIdParameter.text)) {
							System.out.println("Redefinition of field " + $otherIdParameter.text + "at line" + $otherIdParameter.line);
							System.exit(0);
						}
						
						FieldNode otherField = new FieldNode($otherIdParameter.text, $ot.ast);
						
						STentry otherFieldEntry = null;
				        
						/*
				 		 * Se nome di campo è già presente non lo considero errore: è overriding
				 		 * sostituisco nuova STentry alla vecchia preservando
				 		 * l'offset che era nella vecchia STentry
				 		 */ 
				        if (virtualTable.containsKey($otherIdParameter.text)) {
				        	STentry superEntry = virtualTable.get($otherIdParameter.text);	
							
							/*
		        	 		 * Non consento l'overriding di un campo con un metodo 
		        	 		 */ 				        	
				        	if (superEntry.isMethod()) {
								System.out.println("Field id "+ $otherIdParameter.text+" at line "+$otherIdParameter.line+" is overriding a method");
								stErrors++;	    				        		
				        	} else {
				        		int superOffset = superEntry.getOffset();
				            	otherFieldEntry = STentry.getEntry(nestingLevel, superOffset);	
				            	
				            	/* Ottimizzazione */
								otherField.setOffset(superOffset);
	
							   /*
								* Per i campi aggiorno array allFields settando
								* la posizione -offset-1 in base all'offset dell'STentry
			            	 	* (Nel nostro layout l'offset del primo campo è -1)
			            	 	*/
				            	classTypeNode.addOverrideField((-superOffset-1), field);
								classNode.addOverrideField((-superOffset-1), field);            		
				        	}
				
				     	} else {
				     		classTypeNode.addField((-fieldOffset-1), otherField);
							classNode.addField((-fieldOffset-1), otherField);
							otherField.setOffset(fieldOffset);
				     		otherFieldEntry = STentry.getEntry(nestingLevel, fieldOffset--);
				     		
				     	}
				     	
				     	otherFieldEntry.setType($ot.ast);
						virtualTable.put($otherIdParameter.text, otherFieldEntry);
							            	            
				}	
				
			)* )? RPAR
			 
			CLPAR
				// Inizio metodi      
			    ( FUN idMethod = ID COLON methodType = type
					{
						/* Ottimizzazione:
           				 * - Controllare che i nomi dei metodi non siano già stati assegnati.
           				 */
						if (classDeclarations.contains($idMethod.text)) {
							System.out.println("Redefinition of field " + $idMethod.text + "at line" + $idMethod.line);
							System.exit(0);
						}
						
						ArrayList<Node> parameters = new ArrayList<>();
						ArrayList<Node> declarations = new ArrayList<Node>();
						
						//inserimento di ID nella symtable
						MethodNode methodNode = new MethodNode($idMethod.text,$methodType.ast);  
		       
						STentry methodEntry = null;
						
						/*
				 		 * Se nome di metodo è già presente non lo considero errore: è overriding
				 		 * sostituisco nuova STentry alla vecchia preservando
				 		 * l'offset che era nella vecchia STentry
				 		 */ 
				 		 if (virtualTable.containsKey($idMethod.text)) {
						  STentry superEntry = virtualTable.get($idMethod.text);
						 
						 /*
		        	 	  * Non consento l'overriding di un metodo con un campo
		        	 	  */ 		
						  if (!superEntry.isMethod()) {
						    System.out.println("Method id " + $idMethod.text + " at line " + $idMethod.line + " is overriding a field");
						    stErrors++;
						  } else {
						    int superOffset = superEntry.getOffset();

							methodNode.setOffset(superOffset);
						    methodEntry = STentry.getMethodEntry(nestingLevel, superOffset);  
					
		            	   /*
		            		* Per i metodi aggiorno array allFields settando
		            		* la posizione in base all'offset dell'STentry
		            	 	* (Nel nostro layout l'offset del primo metodo è 0)
		            	 	*/
							classTypeNode.addOverrideMethod(superOffset, methodNode);
							classNode.addOverrideMethod(superOffset, methodNode);
						  }
						} else {
						 	methodNode.setOffset(methodOffset);
						 	
							classTypeNode.addMethod(methodOffset, methodNode);
							classNode.addMethod(methodOffset, methodNode);
		
							methodEntry = STentry.getMethodEntry(nestingLevel, methodOffset);
		
							methodOffset = methodOffset + 1;
						}
						
						virtualTable.put($idMethod.text, methodEntry);
		
						//creare una nuova hashmap per la symTable
				
					   /*
	    				* Scope entry:
	    				* - aggiungo una nuova tabella al fronte della lista
	    				* - incremento il nesting level per entrare in un nuovo scope
	    				*/
		        		HashMap<String,STentry> hmn = new HashMap<> ();
		        		symTable.add(hmn);
		        		nestingLevel++;

		        	}       		
		      		LPAR
		      			(parameterId = ID COLON parameterType = hotype
		      				
		      				{ 
							  int parameterOffset=1;
			                  parameters.add($parameterType.ast);
			                  
			                  ParNode methodPar = new ParNode($parameterId.text,$parameterType.ast); //creo nodo ParNode
			                  methodNode.addPar(methodPar);		
			                  
			                  STentry parameterEntry = STentry.getEntry(nestingLevel, parameterOffset);
			                  
							  /*
							   * Per i tipi funzionali l'offset vale doppio:
			                   * - indirizzo dell'AR della dichiarazione della funzione
			                   * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
			                   */ 
			                  if ($parameterType.ast.getClass().equals(ArrowTypeNode.class)) {
 								parameterOffset = parameterOffset + 2;
 	  						  } else {
 	  						  	parameterOffset = parameterOffset + 1;
 	  						  }
			                  
			                  
			                  parameterEntry.setType($parameterType.ast);
			                  if ( hmn.put($parameterId.text,parameterEntry) != null  ) {
			                   System.out.println("Parameter id "+$parameterId.text+" at line "+$parameterId.line+" already declared");
			                   stErrors++;
			                  }   
			                  
		          			}
		      				
		      				(COMMA otherParameterId = ID COLON otherParameterType = hotype
		      					
		      					{
		      							parameters.add($otherParameterType.ast);
//		      							declarations.add($otherParameterType.ast);
			                  			ParNode otherMethodPar = new ParNode($otherParameterId.text,$otherParameterType.ast); //creo nodo ParNode
			                  			methodNode.addPar(otherMethodPar);		  		                  
			                  
										STentry otherParameterEntry = STentry.getEntry(nestingLevel, parameterOffset);
										
										if ($otherParameterType.ast.getClass().equals(ArrowTypeNode.class)) {
             								parameterOffset = parameterOffset + 2;
             	  						} else {
             	  							parameterOffset = parameterOffset + 1;
             	  						}
										
										otherParameterEntry.setType($otherParameterType.ast);
		
										if ( hmn.put($otherParameterId.text, otherParameterEntry) != null  ) {
											System.out.println("Parameter id "+$otherParameterId.text+" at line "+$otherParameterId.line+" already declared");
											stErrors++;
			                  			}   
		      					}
		      					
		      				)*
		      			)?
	      			RPAR
      		
		      		(LET
		      			{
		      				ArrayList<Node> varList = new ArrayList<>();
		      				int varOffset = -2;
		      			}
	
		      			(VAR varId = ID COLON varType = type ASS varExp = exp SEMIC 
	    	  				{
		      					VarNode v = new VarNode($varId.text,$varType.ast,$varExp.ast);  
								methodNode.addDec(v);
								HashMap<String,STentry> table = symTable.get(nestingLevel);
		     
								STentry varEntry = STentry.getEntry(nestingLevel, varOffset);
								
								varEntry.setType($varType.ast);
		     
								if ( table.put($varId.text,entry) != null  ) {
		      						System.out.println("Var id "+$varId.text+" at line "+$varId.line+" already declared");
									stErrors++;
								}
		     
								varOffset = varOffset - 1;  
		    				}
	      				)+
	      		 	IN)? methodExp = exp 
		      		 	{
			      			methodNode.addBody($methodExp.ast);
			      			              		
							methodEntry.setType(new ArrowTypeNode(parameters, $methodType.ast));
			      		
			      		    /*
	  	 					 * Scope exit:
	  	 					 * - Rimuovo la HashMap al nesting level corrente
	  	 					 * - Decremento il nesting level per ripristinarlo a quello dello scope più esterno rispetto a quello correnteF
	  	 					 */
			              	symTable.remove(nestingLevel);
					        nestingLevel = nestingLevel - 1;
			      		}      		
	    		SEMIC)*
      
      		CRPAR
      			{
      			   /*
					* Scope exit:
					* - Rimuovo la HashMap al nesting level corrente
					* - Decremento il nesting level per ripristinarlo a quello dello scope più esterno rispetto a quello correnteF
					*/
      				symTable.remove(nestingLevel);
 					nestingLevel = nestingLevel - 1;
      	     	
			  		$classList.add(classNode);
  				}
  )+ 
; 

declist	returns [ArrayList<Node> astlist]        
	: {$astlist= new ArrayList<Node>() ;
	   int offset = -2; 
	   if (nestingLevel == 0) {
			offset = globalOffset;
	   }
	   }      
	  ( (
            VAR i=ID COLON ht=hotype ASS e=exp 
            {VarNode v = new VarNode($i.text,$ht.ast,$e.ast);  
             $astlist.add(v);                                 
             HashMap<String,STentry> hm = symTable.get(nestingLevel);
             
             STentry entry = STentry.getEntry(nestingLevel, offset);
             entry.setType($ht.ast);
             
             if ( hm.put($i.text,entry) != null  ) {
              System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
              stErrors++;
             }
             
 			/*
			 * Per i tipi funzionali l'offset vale doppio:
             * - indirizzo dell'AR della dichiarazione della funzione
             * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
             */ 
             if (v.getSymType() instanceof ArrowTypeNode) {
             	offset = offset - 2;
             } else {
             	offset = offset - 1;
             }  
            }  
      |  
            FUN i=ID COLON t=type
              {//inserimento di ID nella symtable
               FunNode f = new FunNode($i.text,$t.ast);      
               $astlist.add(f);                              
               HashMap<String,STentry> hm = symTable.get(nestingLevel);
               ArrayList<Node> declarations = new ArrayList<Node>();
               
               STentry entry = STentry.getEntry(nestingLevel, offset); 

               offset = offset - 2;
             	
               if (hm.put($i.text,entry) != null){
                System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
                stErrors++;
               }

			   /*
				* Scope entry:
				* - aggiungo una nuova tabella al fronte della lista
				* - incremento il nesting level per entrare in un nuovo scope
				*/
                HashMap<String,STentry> hmn = new HashMap<String,STentry>();
                symTable.add(hmn);
                nestingLevel++;
                } 
              LPAR {int parameterOffset = 1;}
                (fid=ID COLON fty=hotype
                  { 
                  declarations.add($fty.ast);
                  ParNode fpar = new ParNode($fid.text,$fty.ast); //creo nodo ParNode
                  f.addPar(fpar);                                 //lo attacco al FunNode con addPar			  		                  

				 /*
			 	  * Per i tipi funzionali l'offset vale doppio:
                  * - indirizzo dell'AR della dichiarazione della funzione
                  * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
             	  */ 
                  if ($fty.ast instanceof ArrowTypeNode) {
             			parameterOffset = parameterOffset + 1;
             	  }
				                    
                  STentry parameterEntry = STentry.getEntry(nestingLevel, parameterOffset++);
                  
                  parameterEntry.setType($fty.ast);
                  if ( hmn.put($fid.text,parameterEntry) != null  ) {
                   System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
                   stErrors++;
                  }                  
                }
                  (COMMA id=ID COLON ty=hotype
                    {
                    declarations.add($ty.ast);
                    ParNode par = new ParNode($id.text,$ty.ast);
                    f.addPar(par);
                    
				   /*
			 	    * Per i tipi funzionali l'offset vale doppio:
                    * - indirizzo dell'AR della dichiarazione della funzione
                    * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
             	    */ 
                   	if ($ty.ast instanceof ArrowTypeNode) {
             			parameterOffset = parameterOffset + 1;
             	  	}
					
                    STentry otherParameterEntry = STentry.getEntry(nestingLevel, parameterOffset++);
                                        
                  	otherParameterEntry.setType($ty.ast);
                    if ( hmn.put($id.text,otherParameterEntry) != null  ) {
                     System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
                     stErrors++;
                    }
                    
                    }                    
                  )*
                )? 
              RPAR {
            	ArrowTypeNode symType = new ArrowTypeNode(declarations,$t.ast);
            	entry.setType(symType);
            	f.setSymType(symType);
            }
              (LET d=declist IN {f.addDec($d.astlist);})? e=exp
              {              	
	               f.addBody($e.ast);
				  
				  /*
		  	 	   * Scope exit:
		  	 	   * - Rimuovo la HashMap al nesting level corrente
		  	 	   * - Decremento il nesting level per ripristinarlo a quello dello scope più esterno rispetto a quello correnteF
		  	 	   */	
	               symTable.remove(nestingLevel--);    
              }
      ) SEMIC
    )+          
	;
	
// Oltre a tipi di base (type) anche tipi funzionali (hotype)	
hotype returns [Node ast]
	: t=type {$ast = $t.ast;}
    | a=arrow {$ast = $a.ast;}
        ;	
	
type returns [Node ast]
  : INT  {$ast = new IntTypeNode();}
  | BOOL {$ast = new BoolTypeNode();} 
  | id = ID {$ast = new RefTypeNode($id.text);}
	;	

// Type come tipo di ritorno e tipi hotype per i parametri
arrow returns [Node ast]
	: LPAR {ArrayList arguments = new ArrayList<Node>();}
		(h=hotype {arguments.add($h.ast);}
			(COMMA hot=hotype {arguments.add($hot.ast);})*
		)? RPAR ARROW t=type
			{$ast = new ArrowTypeNode(arguments, $t.ast);}; 

exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (PLUS l=term
 	     {$ast= new PlusNode ($ast,$l.ast);}
 	     
 	     | MINUS t=term // ToDo
    	     {$ast= new MinusNode($ast,$t.ast);}
         | OR t=term    // ToDo
    	     {$ast= new OrNode ($ast,$t.ast);} 	     
 	    )*
 	;
 	
term	returns [Node ast]
	: f=factor {$ast= $f.ast;}
	    (TIMES l=factor
	     {$ast= new TimesNode ($ast,$l.ast);}
	     
	 	| DIV  f=factor
		  {$ast = new DivNode($ast, $f.ast);}  
		| AND  f=factor
		  {$ast = new AndNode($ast, $f.ast);}  
		    
	    )*
	;
	
factor	returns [Node ast]
	: f=value {$ast= $f.ast;}
	    (EQ l=value 
	     {$ast= new EqualNode ($ast,$l.ast);}
	     
	     // Estensione
	     
	     | GE l=value  // ToDo
	     {$ast = new GreaterEqualNode($ast, $l.ast);}
	     | LE l=value  // ToDo
	     {$ast = new LesserEqualNode($ast, $l.ast);}
	     
	     // Estensione
	     
	    )*
 	;	 	
 
value	returns [Node ast]
	: n=INTEGER   
	  {$ast= new IntNode(Integer.parseInt($n.text));}  
	| TRUE 
	  {$ast= new BoolNode(true);}  
	| FALSE
	  {$ast= new BoolNode(false);}  
	| LPAR e=exp RPAR
	  {$ast= $e.ast;}  
    | NULL
      {$ast = new EmptyNode();}	    
    | NEW newId=ID LPAR {
		boolean isIdInClassTable = this.classTable.containsKey($newId.text);
		
	   /*
        * Recuperiamo la STentry dell'ID della classe
        * accedendo direttamente al livello 0 (ambiente globale)
        * in cui sono memorizzate le dichiarazioni delle classi
        */
        STentry entry = symTable.get(0).get($newId.text);
    	if (entry == null || !isIdInClassTable) {
    		System.out.println($newId.text+" at line "+$newId.line+" not declared");
            stErrors++;              
    	}			
    	
    	List<Node> parameters = new ArrayList<>();
    	
    } (e=exp {
    	parameters.add($e.ast);
    }(COMMA ex=exp {
    	parameters.add($ex.ast);
    })* )? RPAR {
    	$ast = new NewNode($newId.text, entry, parameters);
    }
	| IF x=exp THEN CLPAR y=exp CRPAR 
		   ELSE CLPAR z=exp CRPAR 
	  {$ast= new IfNode($x.ast,$y.ast,$z.ast);}	 
	| PRINT LPAR e=exp RPAR	
	  {$ast= new PrintNode($e.ast);}
	
	| NOT LPAR e=exp RPAR // ToDo
	  {$ast = new NotNode($e.ast);}  
	  
	 | i=ID {
			
           /*
            * Cerchiamo la STentry di ID1 tramite la discesa di livelli
            * della symbol table 
            */
           int j=nestingLevel;
           
           STentry entry=null; 
  
           while (j>=0 && entry==null)
             entry=(symTable.get(j--)).get($i.text);
             
           if (entry==null) {
             	System.out.println("Id "+$i.text+" at line "+$i.line+" not declared");
             	stErrors++;
           }	               
		   $ast= new IdNode($i.text,entry,nestingLevel);
	   }
	   
	   ( LPAR {ArrayList<Node> arglist = new ArrayList<Node>();} 
	   	
	   		(a=exp {arglist.add($a.ast);}
	   			(
	   				COMMA a=exp {arglist.add($a.ast);}
	   			)*
	   		)? RPAR {$ast = new CallNode($i.text, entry, arglist, nestingLevel);}
	        	 | DOT id2=ID LPAR {
	   							List<Node> fields = new ArrayList<>();
	   						  }
	   					(e=exp {fields.add($e.ast);}
	   						(
	   							COMMA e=exp {fields.add($e.ast);}
	   						)*

	   					)? 
	   					
	   					{
							if (entry.getType() instanceof RefTypeNode) {
						   		RefTypeNode typeObject = (RefTypeNode) entry.getType();
						   		String idObject = typeObject.getId();
						   		HashMap<String, STentry> virtualTable = classTable.get(idObject);
						   		
						   		/*
						   		 * Recupero l'STentry di id2 in VirtualTable
  						   		 */						   		
						   		STentry methodEntry = virtualTable.get($id2.text);
						   		if (methodEntry == null) {
									System.out.println("Method "+$id2.text+" at line "+$id2.line+" not declared");
					             	stErrors++;               		
						   		}
						   		
						   		$ast = new ClassCallNode(idObject, $id2.text, entry, methodEntry, fields, nestingLevel);								
							} else {
								System.out.println($i.text + " is not an object. Error at line " + $i.line );
					            stErrors++;               	
							}
						   }
						   RPAR 
	   )?	
	   
 	; 

  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

COLON	: ':' ;
COMMA	: ',' ;
ASS	    : '=' ;
SEMIC   : ';' ;
EQ      : '==' ;
OR	    : '||';
AND	    : '&&';
NOT	    : '!' ;
GE	    : '>=' ;
LE	    : '<=' ;
DIV 	: '/';
MINUS 	: '-';
PLUS	: '+' ;
TIMES	: '*' ;
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE	: 'true' ;
FALSE	: 'false' ;
LPAR 	: '(' ;
RPAR	: ')' ;
CLPAR 	: '{' ;
CRPAR	: '}' ;
IF 	    : 'if' ;
THEN 	: 'then' ;
ELSE 	: 'else' ;
PRINT	: 'print' ; 
LET	    : 'let' ;
IN	    : 'in' ;
VAR	    : 'var' ;
FUN	    : 'fun' ;
CLASS	: 'class' ; 
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;	
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ; 	
DOT		: '.';
 
ID 	    : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ; 
 
WHITESP : (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR     : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
