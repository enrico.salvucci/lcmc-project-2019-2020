package ast;

import java.util.ArrayList;
import java.util.List;
import lib.*;

public class MethodNode extends AbstractDecNode {

	private List<Node> parameters = new ArrayList<>();
	private List<VarNode> declarations = new ArrayList<>();
	private String label;
	private int offset;
	private Node exp;

	public MethodNode(String i, Node t) {
		super(i, t);
		this.parameters = new ArrayList<>();
		this.declarations = new ArrayList<>();
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}

	public void addDec(VarNode d) {
		this.declarations.add(d);
	}

	public void addBody(Node b) {
		exp = b;
	}
	
	public void setSymType(Node symType) {
		this.type = symType;
	}
	
	public void addPar(ParNode parameter) {
		this.parameters.add(parameter);
	}
	
	@Override
	public Node getSymType() {
		return this.type;
	}
	
	private void setLabel(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public int getOffset() {
		return this.offset;
	}
	
	public String getId() {
		return this.id;
	}
	
	public List<Node> getParameters() {
		return this.parameters;
	}

	@Override
	public String toPrint(String s) {
	    String parlstr="";
	    String declstr="";
	    for (Node par:parameters){parlstr+=par.toPrint(s+"  ");}
	    for (Node dec:declarations){declstr+=dec.toPrint(s+"  ");}
	    return s+"Method:" + id +"\n" 
	    +type.toPrint(s+"  ")
	    +parlstr
	    +declstr
	    +exp.toPrint(s+"  ") ; 
	  }

	public Node typeCheck() throws TypeException {
		for (Node dec : declarations) {
			try {
				dec.typeCheck();
			} catch (TypeException e) {			 
				System.out.println("In MethodNode" + " Type checking error in a declaration: " + e.text);
			}
		}
		
		if (!FOOLlib.isSubtype(exp.typeCheck(), type)) {
			throw new TypeException("In MethodNode" + " Wrong return type for function " + id);
		}
		return null;	
	}

	public String codeGeneration() {
		
		  String declCode="", popDecl="", popParl="";


		  for (Node dec : declarations) {

			    declCode+=dec.codeGeneration();	    

			    DecNode decNode = (DecNode) dec;
			    
			    if (decNode.getSymType() instanceof ArrowTypeNode) {
				    popDecl+="pop\n";
			    }
			    popDecl+="pop\n";		    	
		    	
		  }
		  
		  for (Node par : parameters) {
			  	DecNode parameterNode = (DecNode) par;
			    
			    if (parameterNode.getSymType() instanceof ArrowTypeNode) {
				    popParl+="pop\n";		 
			    }		  
			    popParl+="pop\n";

		  }
		  
		  this.label = FOOLlib.freshMetLabel();
		
		  String code = 
				    this.label +":\n"+ 
				    "cfp\n"+   // (Copia il valore del registro $sp sul registro $fp)
				    		   // Setto il registro $fp al valore dello Stack Pointer
					"lra\n"+   // Il js del ClassCallNode, oltre a saltare, ha messo sul registro $ra l'indirizzo
				    		   // dell'istruzione successiva
				    		   // Con lra memorizziamo il Return Address del codice della chiamata della funzione ottenuto da $ra
				    		   // In ra avevamo (in precendenza) l'indirizzo dell'istruzione successiva
		    		declCode + // Mettiamo il codice delle dichiarazioni locali
		    		exp.codeGeneration() + // Codice generato dal corpo della funzione
		    		"stm\n"+   // Memorizzo il risultato della funzione sul registro $tm
		    				
		    		// Ora puliamo lo stack. Dopo la pulizia il risultato sarà l'unica cosa presente sullo stack.
		    		popDecl+   // Faccio tante pop dallo stack quante sono le dichiarazioni 
		    		"sra\n"+   // Ora mi ritrovo sullo stack il Return Addess, che mi serve per tornare al chiamante
		    				   // Ho dovuto salvarlo per via dell'invariante sui registri
		    				   // Me lo salvo sul registro $ra
		    				
		    		// Nel mio layout il prossimo sullo stack è l'Access Link
		    		"pop\n"+   // Rimuovo l'Access Link dallo stack 
		    		popParl+   // Faccio tante pop quanti sono i parametri
		    		
		    		// Il prossimo valore sullo stack è il Control Link
		    		"sfp\n"+   // Salvo sul registro $fp il valore del Control Link, il frame del chiamante
		    		
		    		// Ora lo stack è pulito
		    		// Posso rimetterci il risultato della funzione salvato su $tm
		    		"ltm\n"+   // Pusho il valore di $tm che contiene il risultato della funzione
		    		"lra\n"+   // Avevamo salvato su $ra il Return Address.
		    				   // Lo ripushamo sullo stack
		    		"js\n";    // js mi consente un salto senza etichetta sul valore presente dullo stack
		  
		  FOOLlib.putCode(code);	  	 
		  
	  return "";
  }
	
	@Override
	public MethodNode cloneNode() throws CloneException {

		ArrayList<Node> parList = new ArrayList<Node>();
		ArrayList<Node> declList = new ArrayList<Node>();
		
		for (Node p : this.parameters) {
			parList.add(p);
		}
		
		for (Node d : this.declarations) {
			declList.add(d);
		}
		
		MethodNode method = new MethodNode(this.id, this.type.cloneNode());
		method.setOffset(this.offset);
		method.addBody(this.exp);
		return method;
		
	}


}