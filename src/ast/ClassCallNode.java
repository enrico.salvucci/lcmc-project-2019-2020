package ast;

import java.util.ArrayList;
import java.util.List;
import lib.*;

public class ClassCallNode implements Node {

	private String idClass;
	private String idMethod;
	private STentry classEntry;
	private STentry methodEntry;
	private List<Node> arguments;
	private int nestingLevel;
	private List<String> dispatchTable;
	private String extendId;
	/* symType è un campo in cui memorizzo l'arrowTypeNode */

	public ClassCallNode(String idClass, String idMethod, STentry classEntry, STentry methodEntry,
			List<Node> arguments, int nestingLevel) {
		this.idClass = idClass;
		this.idMethod = idMethod;
		this.classEntry = classEntry;
		this.methodEntry = methodEntry;
		this.nestingLevel = nestingLevel;
		this.arguments = arguments;
		
		this.dispatchTable = new ArrayList<>();

	}

	public String toPrint(String s) {
		String parlstr="";
		for (Node par:arguments){parlstr+=par.toPrint(s+"  ");};
		return s+"ClassCall:" + idClass + "."+ idMethod + " at nestinglevel "+nestingLevel+"\n"  +	
		classEntry.toPrint(s+"  ") +
		methodEntry.toPrint(s+"  ") + 
		parlstr;
	}


	public Node typeCheck() throws TypeException {
	     
		 ArrowTypeNode t=(ArrowTypeNode) methodEntry.getType(); 
	     List<Node> p = t.getParList();
	     
	     if ( !(p.size() == arguments.size()) ) {
	       throw new TypeException("In ClassCallNode" + " Wrong number of parameters in the invocation of " + idMethod);
	     }
	     
	     for (int i=0; i< arguments.size(); i++) {

	       if ( !(FOOLlib.isSubtype( (arguments.get(i)).typeCheck(), p.get(i)))) {
	    	   throw new TypeException("In ClassCallNode" + " Wrong type for "+(i+1)+"-th parameter in the invocation of " + idMethod);
 			}
	     }
	     return t.getReturnType();
		
	}

	public String codeGeneration() {
		  
		  String methodParameters = "";
		  
		  for (int i = this.arguments.size() - 1 ; i >= 0; i--)
			  methodParameters += this.arguments.get(i).codeGeneration();
		  
		  String getAR = "";
		  for (int i = 0; i < nestingLevel - this.classEntry.getNestingLevel(); i++)
			  getAR += "lw\n";
		  
		  
		  return "lfp\n" // Carico sullo stack il valore del registro di $fp
			   			 // (Control Link - puntatore all'AR del chiamante)
				 + methodParameters // genera, in ordine inverso, il codice dei metodi
				
				// Nel lostro Layout ora c'è l'AL
				// L'AL deve puntare all'AR dello scope che include il corpo della classe
				// ovvero lo scope che contiene la dichiarazione della classe stessa

				 + getCode(this.classEntry.getOffset(), getAR) // Ottengo l'indirizzo dell'AccessLink,
				 											   // che equivale all'indirizzo dell'Object Pointer
					// Duplico il contenuto della cima dello stack,
					// ovvero l'indirizzo dell'Access Link ottenuto
				 	//
				 	// pusho sullo stack il contenuto dell'indirizzo ottenuto:
				 	// ovvero l'indirizzo del dispatch pointer
					+ "stm\n"
					+ "ltm\n"
					+ "ltm\n"

				 + "lw\n"	// Pusho sullo stack il contenuto dell'indirizzo ottenuto,
				 			// ovvero l'indirizzo del primo metodo dichiarato.
			     + "lw\n"					  
				 + "push " + this.methodEntry.getOffset() + "\n"
				 + "add\n"	// Sommo l'indirizzo ottenuto con l'offset in modo
				 			// da raggiungere la dichiarazione del metodo ID2
				 + "lw\n"	// Pusho sullo stack l'indirizzo dell'AR del metodo
				 + "js\n";	// Salto all'indirizzo dell'AR del metodo
		  		// Quando salto js, come effetto collaterale, js mette in $ra
		  		// l'indirizzo dell'istruzione successiva (in modo da poter tornare indietro)

	}

	private String getCode(int offset, String activationRecordAddress) {
		
		 return "push "+ offset +"\n"+ //pusho sullo stack l'offset dell'oggetto	
		  "lfp\n"+ activationRecordAddress + //pusho AR sullo stack
		  "add\n"+ //sommo l'offset a getAR
		  "lw\n"; //carico sullo stack il valore dell'indirizzo dell'AR ottenuto => abbiamo settato AL									  

	}
	

}

