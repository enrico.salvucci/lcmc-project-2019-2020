package ast;

import java.util.ArrayList;
import java.util.List;
import lib.*;

public class ClassNode extends AbstractDecNode {

	/*
	 * Contiene tutti i campi, compresi quelli ereditati
	 */
	private List<FieldNode> fields = new ArrayList<>();
	/*
	 * Contiene tutti i metodi, compresi quelli ereditati
	 */
	private List<MethodNode> methods = new ArrayList<>();
	private String extendId;
	private STentry superEntry;
	/* symType è un campo in cui memorizzo l'arrowTypeNode */

	public ClassNode(String id, Node type) {
		super(id, type);
	}

	public ClassNode(String id, Node type, List<FieldNode> fields, List<MethodNode> methods) {
		super(id, type);
		this.fields = fields;
		this.methods = methods;
	}

	
	public void setSuperEntry(STentry superEntry) {
		this.superEntry = superEntry;
	}

	public void setExtendId(String extendId) {
		this.extendId = extendId;
	}

	public List<MethodNode> getMethods() {
		return this.methods;
	}

	public List<FieldNode> getFields() {
		return this.fields;
	}

	public void addField(int offset, FieldNode field) {
		this.fields.add(offset, field);
	}

	public void addMethod(int offset, MethodNode method) {
		this.methods.add(offset, method);
	}
	
	public void addOverrideField(int offset, FieldNode field) {
		this.fields.set(offset, field);
	}
	
	public void addOverrideMethod(int superOffset, MethodNode meth) {
		this.methods.set(superOffset, meth);
	}
	
	public void setFields(List<FieldNode> field) {
//		this.fields.clear();
		this.fields.addAll(field);
	}

	public void setMethods(List<MethodNode> method) {
//		this.methods.clear();
		this.methods.addAll(method);
	}

	@Override
	public String toPrint(String indent) {
		String extendStr = (extendId == null) ? "" : " extends " + extendId;
		String fieldlstr = ""; 
		String methodlstr = "";

		for(Node cl: this.fields) {
			fieldlstr += cl.toPrint(indent+"  ");
		}

		for(Node dec: this.methods) {
			methodlstr += dec.toPrint(indent+"  ");
		}

		return indent + "Class "+ id +
				extendStr + 
				"\n" + fieldlstr + methodlstr;	
	}

	@Override
	public Node typeCheck() throws TypeException {

		if (this.superEntry != null) {
			ClassNode superClass = (ClassNode) this.superEntry.getType();
			List<MethodNode> superMethods = superClass.getMethods();
			List<FieldNode> superFields = superClass.getFields();

			/*
			 * Controlla che il tipo alla stessa posizione (i)
			 * in methods e superMethods sia lo stesso oppure sottotipo uno dell'altro
			 */
			for (int i = 0; i < this.methods.size(); i++) {
				/* 
				 * Ottimizzazione pag 62
				 * Controllo la correttezza del subtyping solo per
				 * metodi su cui è stato fatto overriding 
				 */
				int superMethodOffset = this.methods.get(i).getOffset();
				
				if (superMethodOffset < superMethods.size()) {
				/* Fine Ottimizzazione */
					if (FOOLlib.isSubtype(this.methods.get(i), superMethods.get(i))) {
						this.methods.get(i).typeCheck();
					} else {
						throw new TypeException("In ClassNode - Type checking error in a method: " + i + "-th method isn't subtype of the " + (i) + "-th method of the inherited class");
					}
				}
			}

			/*
			 * Controlla che il tipo alla stessa posizione (i)
			 * in fields e superFields sia lo stesso oppure sottotipo uno dell'altro
			 */
			for (int i = 0; i < this.fields.size(); i++) {
				
				/* 
				 * Ottimizzazione pag 62
				 * Controllo la correttezza del subtyping solo per
				 * campi su cui è stato fatto overriding 
				 */
				int superFieldOffset = - this.fields.get(i).getOffset() - 1;
				
				if (superFieldOffset < superFields.size()) {
				/* Fine Ottimizzazione */
				
					if (FOOLlib.isSubtype(this.fields.get(i), superFields.get(i))) {
						throw new TypeException("In ClassNode - Type checking error in a field: " + i + "-th field isn't subtype of the " + (i) + "-th field of the inherited class");
					}

				}
				
			}
		} else {
			for (Node node : this.methods) {
				node.typeCheck();
			}
		}

		return null;
	}

	@Override
	public String codeGeneration() {
		final List<String> dispatchTable = superEntry != null ?
				/*
				 * Dato che l'offset iniziale delle dichiarazioni delle classi è -2, 
				 * la corretta poszione della sua dichiarazione nelle Dispatch Table
				 * sarà - superOffset - 2
				 */
				new ArrayList<>(FOOLlib.dispatchTables.get(- superEntry.getOffset() - 2)) :
				new ArrayList<>();
		
		FOOLlib.dispatchTables.add(dispatchTable);

		this.methods.stream().forEachOrdered(m -> {
			m.codeGeneration();
			dispatchTable.add(m.getOffset(), m.getLabel());
		});

		/*
		 * Allochiamo la Dispatch Table della classe sullo heap
		 * lasciando il dispatch pointer sullo stack
		 */
		return dispatchTable.stream()
				.map(methodLabel -> "push " + methodLabel + "\n"
						+ "lhp\n"	// Pusho sullo stack l'indirizzo attuale dello heap
									// caricandolo dal registro $hp
						+ "sw\n"	// Faccio due cose:
									// - Pop dell'indirizzo ottenuto (indirizzo dello heap)
									// - Memorizzo l'etichetta del metodo nello heap
						+ "lhp\n"	
						+ "push 1\n"
						+ "add\n"	// Incremento l'indirizzo dello heap al quale memorizzare
									// l'etichetta del prossimo metodo
						+ "shp\n"	// Memorizziamo l'indirizzo dello heap ottenuto nel registro $hp
									// in modo da lasciare lo stack come l'abbiamo trovato (invariante)
					)
				.reduce("lhp\n", (partial, codeString) -> partial + codeString);

	}

}