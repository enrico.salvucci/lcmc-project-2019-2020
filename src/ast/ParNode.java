package ast;

import java.util.ArrayList;

import lib.CloneException;

public class ParNode extends AbstractDecNode {
  
  public ParNode (String id, Node t) {
	  super(id, t);
  }
  
  public String toPrint(String s) {
	   return s+"Par:" + id +"\n"
			   +type.toPrint(s+"  "); 
  }

  //non utilizzato
  public Node typeCheck() {return null;}
   
  //non utilizzato
  public String codeGeneration() {return "";}
  
	@Override
	public Node cloneNode() throws CloneException {
		return new ParNode(this.id, this.type.cloneNode());
	}

}  