package ast;

import java.util.ArrayList;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class OrNode implements Node {

  private Node firstNode;
  private Node secondNode;
  
  public OrNode(Node firstNode, Node secondNode) {
   this.firstNode = firstNode;
   this.secondNode = secondNode;
  }
  
  public String toPrint(String s) {
   return s+ "And:" + this.firstNode.toPrint(s + "  ") + "\n"
		   			+ this.secondNode.toPrint(s + "  ");
  }
  
  public Node typeCheck() throws TypeException {
	  if (!(FOOLlib.isSubtype(this.firstNode.typeCheck(), new BoolTypeNode()) &&
		  this.secondNode.typeCheck() instanceof BoolTypeNode) ) {
		  throw new TypeException("Not both values are Boolean");
	  } else {
		  return new BoolTypeNode();
	  }
  }
	  
  public String codeGeneration() { 
	return this.firstNode.codeGeneration() + "\n" +
			"push 1 \n" +
			"beq l1 \n" +
			this.secondNode.codeGeneration() + "\n" +
			"push 0 \n" +
			"beq l2 \n" +
			"l1: \n" +
			"push 1 \n" +
			"b l3 \n" +
			"l2: \n" +
			"push 0 \n" +
			"l3:";
  }
  
	@Override
	public Node cloneNode() throws CloneException {

		return new OrNode(this.firstNode.cloneNode(), this.secondNode.cloneNode());
		
	}
      
}  