package ast;

import lib.*;

public class IdNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		nestingLevel = nl;
		entry = st;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ");
	}

	/*
	 * Per Higher Order ammettiamo anche il caso di ArrowTypeNode 
	 */
	public Node typeCheck() throws TypeException {
		
		/*
		 * Accorgimenti
		 * ID può essere di tipo funzionale ma non deve essere un metodo
		 */
		
		/*
		 * if (entry.getType() instanceof ArrowTypeNode) throw new
		 * TypeException("Wrong usage of function identifier "+id);
		 */
		if (!this.entry.isMethod() && !this.isClassId(entry)) {
			return entry.getType();			
		}		
				
		throw new TypeException("In IdNode" + "Wrong usage of id identifier "+id);
	}

	public String codeGeneration() {

		String getAR = "";
		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++)
			getAR += "lw\n";
				
		// Se l'id è di tipo funzionale ritorno due cose:
		// - l'indirizzo dell'AccessLink, nell'AR in cui f è dichiarata
		// - l'indirizzo di f nella AR ottenuta
		if (!(entry.getType() instanceof ArrowTypeNode)) {
			return getCode(this.entry.getOffset(), getAR);
		} else {
			return getCode(this.entry.getOffset(), getAR) + getCode(this.entry.getOffset() - 1, getAR);
		}
	}
	
	private boolean isClassId(STentry entry) {
		return entry.getType() instanceof ClassTypeNode;
	}
	
	private String getCode(int offset, String activationRecordAddress) {

		return "lfp\n" + // Carico sullo stack il valore del registro di $fp
		   			   	 // (Control Link - puntatore all'AR del chiamante)
				activationRecordAddress + // risalgo la catena statica degli AL per ottenere
										  // l'indirizzo dell'AR che contiene la dichiarazione di id
				"push " + offset + "\n" + // pusho l'offset al quale troviamo la dichiarazione dell'id nell'AR dove è definito
				"add\n" + // Ottengo l'indirizzo effettivo dell'ID in base all'offset
				"lw\n";	  // Memorizzo il contenuto che si trova all'indirizzo ottenuto
	}
	
	@Override
	public Node cloneNode() throws CloneException {
		return new IdNode(this.id, this.entry.cloneNode(), this.nestingLevel);
	}

}