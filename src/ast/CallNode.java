package ast;

import java.util.ArrayList;
import java.util.List;

import lib.*;

public class CallNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;
	private ArrayList<Node> parlist = new ArrayList<Node>();

	public CallNode(String i, STentry st, ArrayList<Node> p, int nl) {
		id = i;
		entry = st;
		parlist = p;
		nestingLevel = nl;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		return s + "Call:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ") + parlstr;
	}

	/*
	 * Il tipo dell'ID deve essere funzionale
	 * (nome di funzione oppure variabile o parametro)
	 */
	public Node typeCheck() throws TypeException {
		if (!(entry.getType() instanceof ArrowTypeNode)) {
			throw new TypeException("In CallNode" + " Invocation of a non-function " + id);
		}

		ArrowTypeNode t = (ArrowTypeNode) entry.getType();
		List<Node> p = t.getParList();
		if (!(p.size() == parlist.size())) {
			throw new TypeException("In CallNode" + " Wrong number of parameters in the invocation of " + id);
		}

		for (int i = 0; i < parlist.size(); i++) {

			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				throw new TypeException(
						"In CallNode" + " Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
			}
		}
		return t.getReturnType();
	}

	public String codeGeneration() {
		String parCode = "", getAR = "";

		for (int i = parlist.size() - 1; i >= 0; i--)
			parCode += parlist.get(i).codeGeneration();

		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++) {
			getAR += "lw\n";
		}

		int offset = entry.getOffset();

		/*
		 * Accorgimenti
		 * Controllo se ID è un metodo.
		 */
		if (entry.isMethod()) { // se è metodo => ritorno codice di estensione Object Oriented
			// aggiungo un salto in più
			getAR += "lw\n";
			
			return "lfp\n" 	// Carico sullo stack il valore del registro di $fp
							// (Control Link - puntatore all'AR del chiamante)
				+ parCode 	// genera, in ordine inverso, il codice dei parametri
				
				// Nel lostro Layout ora c'è l'AL
				// L'AL deve puntare all'AR dello scope che include il corpo di f
				// ovvero lo scope che contiene la dichiarazione di f
				
				+ getCode(offset, getAR) 	 // Ottengo l'indirizzo dell'AccessLink, nell'AR in cui f è dichiarata
				// Tale indirizzo è l'Object Pointer
				
				+ "stm\n"
				+ "ltm\n"
				+ "ltm\n"	// Duplico il contenuto in cima allo stack
						// ovvero l'indirizzo dell'Access Link
				+ "push " + offset + "\n"
				+ "add\n"	// Calcolo l'indirizzo del Dispatch Pointer tramite l'offset
				+ "js\n"; 	// Quando salto js, come effetto collaterale, js mette in $ra
						  	// l'indirizzo dell'istruzione successiva (in modo da poter tornare indietro)

			
		} else {
			return "lfp\n" 	// Carico sullo stack il valore del registro di $fp
							// (Control Link - puntatore all'AR del chiamante)
				+ parCode 	// genera, in ordine inverso, il codice dei parametri
				
				// Nel lostro Layout ora c'è l'AL
				// L'AL deve puntare all'AR dello scope che include il corpo di f
				// ovvero lo scope che contiene la dichiarazione di f
				
				+ getCode(offset, getAR) 	 // Ottengo l'indirizzo dell'AccessLink, nell'AR in cui f è dichiarata
				+ getCode(offset - 1, getAR) // Ottengo l'indirizzo di f nella AR ottenuta
				+ "js\n"; 	// Quando salto js, come effetto collaterale, js mette in $ra
						  	// l'indirizzo dell'istruzione successiva (in modo da poter tornare indietro)
		}																				
	}	

	public String getCode(int offset, String activationRecordAddress) {
		return  "lfp\n" 				  // Prendo l'Access Link dell'AR corrente e risalgo
				+ activationRecordAddress // Risalgo la catena statica per
										  // differenza di nesting level attuale e quello della dichiarazione di f
										  // Ora devo saltare. Siamo nell'AR della funzione
										  // Trovo l'indirizzo su cui saltare tramite l'offset
				+ "push " + offset + "\n"
				+ "add\n"	// Prima sommo la posizione di riferimento con l'offset
				+ "lw\n"; 	// Ottengo (e pusho) il valore all'indirizzo presente sullo stack
						
	}
}