package ast;

public abstract class AbstractDecNode implements DecNode, Node {
	
	protected String id;
	protected Node type;
	
	public AbstractDecNode(String id, Node type) {
		this.id = id;
		this.type = type;
	}
	
	@Override
	public Node getSymType() {
		return this.type;
	}

}
