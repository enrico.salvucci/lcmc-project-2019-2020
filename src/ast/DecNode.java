package ast;

public interface DecNode extends Node {
		
	/*
	 * Ritorna il tipo, messo in symble table,
	 * di un nodo (FunNode, VarNode, ParNode,
	 * MethodNode, ClassNode e FieldNode)
	 */
	public Node getSymType();

}
