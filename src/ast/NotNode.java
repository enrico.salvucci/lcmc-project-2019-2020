package ast;

import java.util.ArrayList;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class NotNode implements Node {

  private Node node;
  
  public NotNode(Node node) {
   this.node = node;
  }
  
  public String toPrint(String s) {
   return s+ "Not:" + this.node.toPrint(s + "  ");
  }
  
  public Node typeCheck() throws TypeException {
	  if (!(FOOLlib.isSubtype(this.node.typeCheck(), new BoolTypeNode()))) {
		  throw new TypeException("Not Boolean value");
	  } else {
		  return new BoolTypeNode();
	  }
  }
	  
  public String codeGeneration() { 
	String l1= FOOLlib.freshLabel();
	String l2= FOOLlib.freshLabel();

	return this.node.codeGeneration() + "\n" +
			"push 1 \n" +
			"beq " + l1 + "\n" +
			"push 1\n" +
			"b " + l2 + "\n" +
			l1 + ": \n" +
			"push 0\n" +
			l2 + ":\n";
  }
      
  
	@Override
	public Node cloneNode() throws CloneException {
		return new NotNode(this.node.cloneNode());
	}
}  