package ast;

import lib.*;

public class EmptyNode implements Node {

	public EmptyNode() {
	}

	public String toPrint(String indent) {
		return indent + "Null\n";
	}

	public Node typeCheck() throws TypeException {
		return new EmptyTypeNode();
	}

	public String codeGeneration() {
		return "push -1\n";
	}
	
	@Override
	public Node cloneNode() throws CloneException {
		return new EmptyNode();
	}

}