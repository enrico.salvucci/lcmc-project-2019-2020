package ast;

import lib.*;
import java.util.ArrayList;

public class ProgLetInNode implements Node {

	private ArrayList<Node> declist;
	private ArrayList<Node> classList;
	private Node exp;

	public ProgLetInNode(ArrayList<Node> classList, ArrayList<Node> d, Node e) {
		this.declist = d;
		this.classList = classList;
		this.exp = e;
	}

	public String toPrint(String indent) {
		String declStr = "";
		String clStr = "";
		for (final Node dec : this.declist) {
			declStr += dec.toPrint(indent + "  ");
		}
		for (final Node cl : this.classList) {
			clStr += cl.toPrint(indent + "  ");
		}
		return indent + "ProgLetIn\n" + clStr + declStr + this.exp.toPrint("  ");
	}

	public Node typeCheck() throws TypeException {
		for (Node dec : declist)
			try {
				dec.typeCheck();
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		return exp.typeCheck();
	}

	public String codeGeneration() {
		String declCode = "";
		String classCode = "";
		for (Node c : classList) {
			classCode = classCode + c.codeGeneration();
		}

		for (Node dec : declist) {
			String d = dec.codeGeneration();
			declCode += d;
		}
		return "push 0\n" + classCode + declCode + exp.codeGeneration() + "halt\n" + FOOLlib.getCode();
	}
}