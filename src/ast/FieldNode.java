package ast;

import lib.*;

public class FieldNode extends AbstractDecNode {

	// Ottimizzazione
	private int offset;
	
	public FieldNode(String id, Node t) {
		super(id, t);
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public int getOffset() {
		return this.offset;
	}

	public String toPrint(String s) {
	    return s+"Field:" + id +"\n"
	        +type.toPrint(s+"  ") ; 
	}

	@Override
	public Node typeCheck() throws TypeException {
		// Non utilizzato
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}
	
	@Override
	public FieldNode cloneNode() throws CloneException {
		return new FieldNode(this.id, this.type.cloneNode());
	}

	@Override
	public String toString() {
		return this.toPrint("");
	}
	
}