package ast;

import lib.CloneException;

public class STentry {

	private int nl;
	private Node type;
	private int offset;
	
	/*
	 * Campo necessario per distinguere ID di funzioni da ID di metodi,
	 * i quali righiedono l'uso di dispatch table quando invocati
	 */
	private boolean isMethod = false;

	private STentry(int n, int o, boolean isMethod) {
		this.nl = n;
		this.offset = o;
		this.isMethod = isMethod;
	}

	public Node getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}

	public int getNestingLevel() {
		return nl;
	}

	public void setType(Node type) {
		this.type = type;
	}

	public static STentry getMethodEntry(int nestingLevel, int offset) {
		return new STentry(nestingLevel, offset, true);
	}

	public static STentry getEntry(int nestingLevel, int offset) {
		return new STentry(nestingLevel, offset, false);
	}

	public boolean isMethod() {
		return this.isMethod;
	}

	public String toPrint(String s) {
		return s + "STentry: nestlev " + Integer.toString(nl) + "\n" + s + "STentry: type\n " + type.toPrint(s + "  ")
				+ s + "STentry: offset " + Integer.toString(offset) + "\n" + s + "isMethod " + isMethod + "\n";

	}

	@Override
	public String toString() {
		return "STentry";
	}

	public STentry cloneNode() throws CloneException {
		STentry entry = new STentry(this.nl, this.offset, this.isMethod);
		entry.setType(this.type.cloneNode());
		return entry;
	}
	
}