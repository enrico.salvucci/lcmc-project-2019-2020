package ast;

import lib.*;

public class IfNode implements Node {

	private Node cond;
	private Node thenNode;
	private Node elseNode;

	public IfNode(Node c, Node thenNode, Node elseNode) {
		cond = c;
		this.thenNode = thenNode;
		this.elseNode = elseNode;
	}

	public String toPrint(String s) {
		return s + "If\n" + cond.toPrint(s + "  ") + thenNode.toPrint(s + "  ") + elseNode.toPrint(s + "  ");
	}

	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(cond.typeCheck(), new BoolTypeNode())))
			throw new TypeException("Non boolean condition in if");
		Node thenNode = this.thenNode.typeCheck();
		Node elseNode = this.elseNode.typeCheck();

		/*
		 * Ottimizzazione
		 * Rendo possibile utilizzare nei rami then else due espressioni
		 * anche quando hanno un Lower Common Ancestor
		 */
		Node lowerCommonAncestor = FOOLlib.getLowestCommonAncestor(thenNode, elseNode);
		if (lowerCommonAncestor == null) {
			throw new TypeException("Incompatible types in then-else branches");
		}
		
		/* Prima di ottimizzazione	*/
/*		if (FOOLlib.isSubtype(t, e))
			return e;
		if (FOOLlib.isSubtype(e, t))
			return t;
			
*/
//		throw new TypeException("Incompatible types in then-else branches");
		return lowerCommonAncestor;
	}

	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return cond.codeGeneration() + "push 1\n" + "beq " + l1 + "\n" + elseNode.codeGeneration() + "b " + l2 + "\n" + l1
				+ ": \n" + thenNode.codeGeneration() + l2 + ": \n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new IfNode(this.cond.cloneNode(), this.thenNode.cloneNode(), this.elseNode.cloneNode());
	}

}