package ast;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class AndNode implements Node {

  private Node firstNode;
  private Node secondNode;
  
  public AndNode(Node firstNode, Node secondNode) {
   this.firstNode = firstNode;
   this.secondNode = secondNode;
  }
  
  public String toPrint(String s) {
   return s+ "And:" + this.firstNode.toPrint(s + "  ") + "\n"
		   			+ this.secondNode.toPrint(s + "  ");
  }
  
  public Node typeCheck() throws TypeException {
	  if (!(FOOLlib.isSubtype(this.firstNode.typeCheck(), new BoolTypeNode())
		  && FOOLlib.isSubtype(this.secondNode.typeCheck(), new BoolTypeNode()))) {
		  throw new TypeException("Not both values are Boolean");
	  } else {
		  return new BoolTypeNode();
	  }
  }
	  
  public String codeGeneration() { 
	String l1= FOOLlib.freshLabel();
	String l2= FOOLlib.freshLabel();
	String l3= FOOLlib.freshLabel();

	return this.firstNode.codeGeneration() + "\n" +
			"push 0 \n" +
			"beq " + l3 + "\n" +
			"push 1 \n" +			
			this.secondNode.codeGeneration() + "\n" +
			"beq " + l1 + "\n" +
			l3 + ":\n" +
			"push 0 \n" +
			"b " + l2 + "\n" +
			l1 + ":\n" + 
			"push 1 \n" +
			l2 + ":\n";
  }
  
  @Override
  public Node cloneNode() throws CloneException {
	return new AndNode(firstNode.cloneNode(), secondNode.cloneNode());
	  
  }
      
}  