package ast;

import java.util.ArrayList;

import lib.CloneException;
import lib.TypeException;

public class RefTypeNode implements Node {

	private String id;

	public RefTypeNode(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}

	public String toPrint(String indent) {
		return indent + "RefTypeNode " + id + "\n";
	}

	@Override
	public Node typeCheck() throws TypeException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String codeGeneration() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Node cloneNode() throws CloneException {
		return new RefTypeNode(this.id);
	}

}