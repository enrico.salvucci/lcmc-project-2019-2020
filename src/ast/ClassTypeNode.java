package ast;

import java.util.List;

import lib.CloneException;

import java.util.ArrayList;

public class ClassTypeNode implements Node {

	/*
	 * Contiene tutti i campi, compresi quelli ereditati
	 */
	private List<FieldNode> fields = new ArrayList<>();

	/*
	 * Contiene tutti i metodi, compresi quelli ereditati
	 */
	private List<MethodNode> methods = new ArrayList<>();

	public ClassTypeNode() {

	}

	public ClassTypeNode(List<FieldNode> fields, List<MethodNode> methods) {
		super();
		this.fields = fields;
		this.methods = methods;
	}
	
	public void addField(int offset, FieldNode field) {
		this.fields.add(offset, field);
	}
	
	public void addOverrideField(int offset, FieldNode field) {
		this.fields.set(offset, field);
	}
	
	public void addMethod(int offset, MethodNode method) {
		this.methods.add(offset, method);
	}
	
	public void addOverrideMethod(int superOffset, MethodNode meth) {
		this.methods.set(superOffset, meth);
	}
	
	@Override
	public String toPrint(String indent) {
		String fieldstr = "";
		String methodstr = "";

		for (Node n : fields) {
			fieldstr += n.toPrint(indent + "   ");
		}

		for (Node n : methods) {
			methodstr += n.toPrint(indent + "   ");
		}
		return indent + "ClassTypeNode\n" + fieldstr + methodstr;
	}

/*	public void addField(Node field) {
		this.fields.add(field);
	}

	public void addMethod(MethodNode method) {
		this.methods.add(method);
	}
*/
	public List<FieldNode> getFields() {
		return this.fields;
	}

	public List<MethodNode> getMethods() {
		return this.methods;
	}

	// non utilizzato
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node cloneNode() throws CloneException {
		List<FieldNode> clonedFields = new ArrayList<>();
		List<MethodNode> clonedMethods = new ArrayList<>();

		for (FieldNode f : this.fields) {
			clonedFields.add(f.cloneNode());
		}

		for (MethodNode m : this.methods) {
			clonedMethods.add(m.cloneNode());
		}

		return new ClassTypeNode(clonedFields, clonedMethods);
	}
}