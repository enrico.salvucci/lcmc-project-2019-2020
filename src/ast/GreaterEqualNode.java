package ast;
import lib.*;

public class GreaterEqualNode implements Node {

  private Node left;
  private Node right;
  
  public GreaterEqualNode (Node l, Node r) {
   left=l;
   right=r;
  }
  
  public String toPrint(String s) {
   return s+"Greater\n" + left.toPrint(s+"  ")   
                      + right.toPrint(s+"  ") ; 
  }
    
  public Node typeCheck() throws TypeException {
	Node l= left.typeCheck();  
	Node r= right.typeCheck();  
    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) 
    	throw new TypeException("Incompatible types in greater or equal");
    return new BoolTypeNode();
  }
  
  public String codeGeneration() {
    String l1= FOOLlib.freshLabel();
    String l2= FOOLlib.freshLabel();
    String l3= FOOLlib.freshLabel();
	  return left.codeGeneration()+
			 right.codeGeneration()+
			 "bleq " + l1 + "\n" +
			 "b "+l2 +"\n"+
			 l1+": \n"+
			 left.codeGeneration() +
			 right.codeGeneration() +
			 "beq "+ l2 + "\n" +
			 "push 0 \n"+
			 "b "+l3 +"\n"+
			 l2 + ": \n" +
			 "push 1 \n" +
			 l3 + ": \n";
  }
  
	@Override
	public Node cloneNode() throws CloneException {
		return new GreaterEqualNode(left.cloneNode(), right.cloneNode());
	}

}  