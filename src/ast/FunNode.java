package ast;

import java.util.ArrayList;
import lib.*;

public class FunNode extends AbstractDecNode {

	private ArrayList<Node> parlist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node symType;
	private Node exp;

	public FunNode(String i, Node t) {
		super(i, t);
	}

	public void addDec(ArrayList<Node> d) {
		declist = d;
	}

	public void addBody(Node b) {
		exp = b;
	}

	public void addPar(Node p) {
		parlist.add(p);
	}
	
	public void setSymType(Node symType) {
		this.symType = symType;
	}
	
	@Override
	public Node getSymType() {
		return this.symType;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		String declstr = "";
		for (Node dec : declist)
			declstr += dec.toPrint(s + "  ");
		return s + "Fun:" + id + "\n" + type.toPrint(s + "  ") + parlstr + declstr + exp.toPrint(s + "  ");
	}

	public Node typeCheck() throws TypeException {
		for (Node dec : declist) {
			try {
				dec.typeCheck();
			} catch (TypeException e) {
				System.out.println("In FunNode" + " Type checking error in a declaration: " + e.text);
			}
		}
		
		if (!FOOLlib.isSubtype(exp.typeCheck(), type))
			throw new TypeException("In FunNode" + " Wrong return type for function " + id);
		return null;
	}

	public String codeGeneration() {
	  
	  String declCode="", popDecl="", popParl="";

	  for (Node dec:declist) {

		    declCode+=dec.codeGeneration();	    

		    DecNode decNode = (DecNode) dec;
		    
		    if (decNode.getSymType() instanceof ArrowTypeNode) {
			    popDecl+="pop\n";
		    }
		    popDecl+="pop\n";		    	
	    	
	  }
	  
	  for (Node par:parlist) {
		  	DecNode parameterNode = (DecNode) par;
		    
		    if (parameterNode.getSymType() instanceof ArrowTypeNode) {
			    popParl+="pop\n";		 
		    }		  
		    popParl+="pop\n";
	  }

	  
	  String funl=FOOLlib.freshFunLabel();
	  
	  // Il FunNode è una dichiarazione di funzione
	  String code = 
			    funl+":\n"+ 
			    "cfp\n"+   // (Copia il valore del registro $sp sul registro $fp)
			    		   // Setto il registro $fp al valore dello Stack Pointer
				"lra\n"+   // Il js del CallNode, oltre a saltare, ha messo sul registro $ra l'indirizzo
			    		   // dell'istruzione successiva
			    		   // Con lra memorizziamo il Return Address del codice della chiamata della funzione ottenuto da $ra
			    		   // In ra avevamo (in precendenza) l'indirizzo dell'istruzione successiva
	    		declCode + // Mettiamo il codice delle dichiarazioni locali
	    		exp.codeGeneration() + // Codice generato dal corpo della funzione
	    		"stm\n"+   // Memorizzo il risultato della funzione sul registro $tm
	    				
	    		// Ora puliamo lo stack. Dopo la pulizia il risultato sarà l'unica cosa presente sullo stack.
	    		popDecl+   // Faccio tante pop dallo stack quante sono le dichiarazioni 
	    		"sra\n"+   // Ora mi ritrovo sullo stack il Return Addess, che mi serve per tornare al chiamante
	    				   // Ho dovuto salvarlo per via dell'invariante sui registri
	    				   // Me lo salvo sul registro $ra
	    				
	    		// Nel mio layout il prossimo sullo stack è l'Access Link
	    		"pop\n"+   // Rimuovo l'Access Link dallo stack 
	    		popParl+   // Faccio tante pop quanti sono i parametri
	    		
	    		// Il prossimo valore sullo stack è il Control Link
	    		"sfp\n"+   // Salvo sul registro $fp il valore del Control Link, il frame del chiamante
	    		
	    		// Ora lo stack è pulito
	    		// Posso rimetterci il risultato della funzione salvato su $tm
	    		"ltm\n"+   // Pusho il valore di $tm che contiene il risultato della funzione
	    		"lra\n"+   // Avevamo salvato su $ra il Return Address.
	    				   // Lo ripushamo sullo stack
	    		"js\n";    // js mi consente un salto senza etichetta sul valore presente dullo stack
	  
	  FOOLlib.putCode(code);	  
	  
	  // Ritorno due cose:
	  return "lfp \n" +	// - indirizzo (fp) a questo AR
			 "push "+ funl +"\n";	// - etichetta generata
	}
}