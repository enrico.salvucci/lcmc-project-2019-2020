package ast;

import java.util.ArrayList;
import java.util.List;
import lib.*;

public class NewNode implements Node {

	private String id;
	private STentry entry;
	private List<Node> arguments = new ArrayList<>();
	/* symType è un campo in cui memorizzo l'arrowTypeNode */

	public NewNode(String id, STentry entry, List<Node> arguments) {
		this.id = id;
		this.entry = entry;
		this.arguments = arguments;
	}

	@Override
	public String toPrint(String indent) {
		String argumentsString = "";

		for (Node arg : arguments) {
			// aggiungo alla stringa
			// il to print di ogni nodo
			argumentsString += arg.toPrint(indent + "  ");
		}

		return indent + "New " + id + "\n" + argumentsString;
	}

	@Override
	public Node typeCheck() throws TypeException {
			 ClassTypeNode classTypeNode = (ClassTypeNode) this.entry.getType();
		     List<FieldNode> fields = classTypeNode.getFields();
		     
		     if ( fields.size() != arguments.size()) {
		    	 throw new TypeException("In NewNode" + " Wrong number of parameters in the invocation of "+id);
		     }
		     
		     for (int i=0; i<arguments.size(); i++) {
		       if ( !(FOOLlib.isSubtype( (arguments.get(i)).typeCheck(), ((FieldNode) fields.get(i)).getSymType()))) {
			    	throw new TypeException("In NewNode" +  " Wrong type for "+(i+1)+"-th parameter in the invocation of "+id);
		       }
		     }

		     return new RefTypeNode(this.id);
		
	}

	@Override
	public String codeGeneration() {

		String code = "";
		  for (int i = this.arguments.size() - 1 ; i >= 0; i--) {		  
			  code = code
					 + this.arguments.get(i).codeGeneration()
					 + "lhp\n"	// Pusho l'indirizzo dello heap
					 + "sw\n"	// Pusho a quell'indirizzo il risultato della code generation
					 					 
					 + "lhp\n"	
					 + "push 1\n"
					 + "add\n" // Incremento di 1 il valore di $hp e ripeto
					 + "shp\n";	// Salvo l'indirizzo incrementato dello heap nel registro $fp
		  }
		
		int offset = this.entry.getOffset();
		code = code + "push " + FOOLlib.MEMSIZE + "\n" // Uso MEMSIZE perchè l'AR in cui
										// è dichiarata la classe è relativa al livello globale
					+ "push " + offset + "\n"	// utilizzo l'offset per ottenere 
												// l'indirizzo del dispatch pointer
					+ "add\n" // Recupero da MEMSIZE + offset il dispatch pointer
					+ "lhp\n" // Pusho l'indirizzo dello heap, contenuto nel registro $hp
					+ "sw\n"  // Pusho a quell'indirizzo il risultato della add			

					+ "lhp\n" // Ritorno l'indirizzo dell'Object Pointer
					
					+ "lhp\n" // Carica sullo stack il valore di $hp
					+ "push 1\n"
					+ "add\n" // e incrementa l'indirizzo dello $hp
					+ "shp\n"; // Memorizzo il nuovo indirizzo sul registro $hp
		return code;
	}
	
	@Override
	public Node cloneNode() throws CloneException {
		ArrayList<Node> args = new ArrayList<>();
		
		for (Node a : args) {
			args.add(a.cloneNode());
		}
		
		return new NewNode(this.id, this.entry.cloneNode(), args);
	}

}