package ast;

import lib.*;

public class EmptyTypeNode implements Node {

	public EmptyTypeNode() {
	}

	public String toPrint(String indent) {
		return indent + "EmptyTypeNode\n";
	}

	public Node typeCheck() throws TypeException {
		return null;
	}

	public String codeGeneration() {
		return null;
	}
	
	@Override
	public Node cloneNode() throws CloneException {
		return new EmptyTypeNode();
	}

}