package ast;
import java.util.ArrayList;

import lib.*;

public class VarNode extends AbstractDecNode {

  private Node exp;
  
  public VarNode (String i, Node t, Node v) {
	  super(i, t);
	  exp=v;
  }
  
  public String toPrint(String s) {
	   return s+"Var:" + id +"\n"
			   +type.toPrint(s+"  ")  
			   +exp.toPrint(s+"  ");
  }
  
  public Node typeCheck() throws TypeException {
	if (! FOOLlib.isSubtype(exp.typeCheck(),type)) 
		throw new TypeException("Incompatible value for variable "+id);
  return null;
  }
    
  public String codeGeneration() {
	return exp.codeGeneration();
  }
  
	@Override
	public Node cloneNode() throws CloneException {
		return new VarNode(this.id, this.type, this.exp.cloneNode());
	}

}  