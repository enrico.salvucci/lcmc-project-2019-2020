package ast;

import lib.CloneException;

public class IntTypeNode implements Node {
  
  public IntTypeNode () {
  }
  
  public String toPrint(String s) {
	   return s+"IntType\n";  
  }

  //non utilizzato
  public Node typeCheck() {return null;}

  //non utilizzato
  public String codeGeneration() {return "";}

  @Override
  public Node cloneNode() throws CloneException {
	  return new IntTypeNode();
  }

}  