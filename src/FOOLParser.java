// Generated from FOOL.g4 by ANTLR 4.7

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;


import ast.*;
import lib.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COLON=1, COMMA=2, ASS=3, SEMIC=4, EQ=5, OR=6, AND=7, NOT=8, GE=9, LE=10, 
		DIV=11, MINUS=12, PLUS=13, TIMES=14, INTEGER=15, TRUE=16, FALSE=17, LPAR=18, 
		RPAR=19, CLPAR=20, CRPAR=21, IF=22, THEN=23, ELSE=24, PRINT=25, LET=26, 
		IN=27, VAR=28, FUN=29, CLASS=30, EXTENDS=31, NEW=32, NULL=33, INT=34, 
		BOOL=35, ARROW=36, DOT=37, ID=38, WHITESP=39, COMMENT=40, ERR=41;
	public static final int
		RULE_prog = 0, RULE_cllist = 1, RULE_declist = 2, RULE_hotype = 3, RULE_type = 4, 
		RULE_arrow = 5, RULE_exp = 6, RULE_term = 7, RULE_factor = 8, RULE_value = 9;
	public static final String[] ruleNames = {
		"prog", "cllist", "declist", "hotype", "type", "arrow", "exp", "term", 
		"factor", "value"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "':'", "','", "'='", "';'", "'=='", "'||'", "'&&'", "'!'", "'>='", 
		"'<='", "'/'", "'-'", "'+'", "'*'", null, "'true'", "'false'", "'('", 
		"')'", "'{'", "'}'", "'if'", "'then'", "'else'", "'print'", "'let'", "'in'", 
		"'var'", "'fun'", "'class'", "'extends'", "'new'", "'null'", "'int'", 
		"'bool'", "'->'", "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COLON", "COMMA", "ASS", "SEMIC", "EQ", "OR", "AND", "NOT", "GE", 
		"LE", "DIV", "MINUS", "PLUS", "TIMES", "INTEGER", "TRUE", "FALSE", "LPAR", 
		"RPAR", "CLPAR", "CRPAR", "IF", "THEN", "ELSE", "PRINT", "LET", "IN", 
		"VAR", "FUN", "CLASS", "EXTENDS", "NEW", "NULL", "INT", "BOOL", "ARROW", 
		"DOT", "ID", "WHITESP", "COMMENT", "ERR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "FOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	int stErrors=0;

	private int nestingLevel = 0;
	private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();

	/*
	 * Il layout degli AR globale, in cui troviamo
	 * le dichiarazioni delle classi, delle variabili, delle funzioni
	 * contiene Control Link 
	 * 			Return Address
	 * e, a offset, -2 la classe/funzioni/variabili
	 */
	private int globalOffset = -2;

	/*
	 * Serve per preservare le dichiarazioni interne ad una classe (campi e metodi)
	 * per renderli accessibili anche in seguito alla dichiarazione della classe
	 */
	private HashMap<String, HashMap<String, STentry>> classTable = new HashMap<>();

	public FOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public Node ast;
		public ExpContext e;
		public CllistContext c;
		public DeclistContext d;
		public TerminalNode SEMIC() { return getToken(FOOLParser.SEMIC, 0); }
		public TerminalNode EOF() { return getToken(FOOLParser.EOF, 0); }
		public TerminalNode LET() { return getToken(FOOLParser.LET, 0); }
		public TerminalNode IN() { return getToken(FOOLParser.IN, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public CllistContext cllist() {
			return getRuleContext(CllistContext.class,0);
		}
		public DeclistContext declist() {
			return getRuleContext(DeclistContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

				   /*
				    * Scope entry:
				    * - creo una nuova tabella
				    * - la aggiungo al fronte della lista
				    */	
				   HashMap<String,STentry> hm = new HashMap<String,STentry> ();
			       symTable.add(hm);
			       
			setState(42);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT:
			case INTEGER:
			case TRUE:
			case FALSE:
			case LPAR:
			case IF:
			case PRINT:
			case NEW:
			case NULL:
			case ID:
				{
				setState(21);
				((ProgContext)_localctx).e = exp();
				((ProgContext)_localctx).ast =  new ProgNode(((ProgContext)_localctx).e.ast);
				}
				break;
			case LET:
				{
				setState(24);
				match(LET);

				      	ArrayList<Node> classList = new ArrayList<>();
				      	ArrayList<Node> declList = new ArrayList<>();
				      
				setState(36);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CLASS:
					{
					setState(26);
					((ProgContext)_localctx).c = cllist();
					 classList.addAll(((ProgContext)_localctx).c.classList);
					setState(31);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==VAR || _la==FUN) {
						{
						setState(28);
						((ProgContext)_localctx).d = declist();
						declList.addAll(((ProgContext)_localctx).d.astlist);
						}
					}

					}
					break;
				case VAR:
				case FUN:
					{
					setState(33);
					((ProgContext)_localctx).d = declist();
					declList.addAll(((ProgContext)_localctx).d.astlist);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(38);
				match(IN);
				setState(39);
				((ProgContext)_localctx).e = exp();

								((ProgContext)_localctx).ast =  new ProgLetInNode(classList, declList, ((ProgContext)_localctx).e.ast);  
							
				}
				break;
			default:
				throw new NoViableAltException(this);
			}

				  	/*
				  	 * Scope exit:
				  	 * - Rimuovo la HashMap al nesting level corrente
				  	 */
				  	symTable.remove(nestingLevel);
				  
			setState(45);
			match(SEMIC);
			setState(46);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CllistContext extends ParserRuleContext {
		public ArrayList<Node> classList;
		public Token idClass;
		public Token idSuperClass;
		public Token idParameter;
		public TypeContext t;
		public Token otherIdParameter;
		public TypeContext ot;
		public Token idMethod;
		public TypeContext methodType;
		public Token parameterId;
		public HotypeContext parameterType;
		public Token otherParameterId;
		public HotypeContext otherParameterType;
		public Token varId;
		public TypeContext varType;
		public ExpContext varExp;
		public ExpContext methodExp;
		public List<TerminalNode> CLASS() { return getTokens(FOOLParser.CLASS); }
		public TerminalNode CLASS(int i) {
			return getToken(FOOLParser.CLASS, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<TerminalNode> EXTENDS() { return getTokens(FOOLParser.EXTENDS); }
		public TerminalNode EXTENDS(int i) {
			return getToken(FOOLParser.EXTENDS, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public CllistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cllist; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterCllist(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitCllist(this);
		}
	}

	public final CllistContext cllist() throws RecognitionException {
		CllistContext _localctx = new CllistContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_cllist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

					((CllistContext)_localctx).classList =  new ArrayList<>();
				
			setState(132); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(49);
				match(CLASS);
				setState(50);
				((CllistContext)_localctx).idClass = match(ID);


						    HashMap<String, STentry> virtualTable = new HashMap<>();
							HashMap<String, STentry> superVirtualTable = new HashMap<>();          	


							ClassTypeNode classTypeNode = new ClassTypeNode();  
							ClassNode classNode = new ClassNode((((CllistContext)_localctx).idClass!=null?((CllistContext)_localctx).idClass.getText():null), classTypeNode);	
							Set<String> classDeclarations = new HashSet<>();
							
							int methodOffset = 0;
							int fieldOffset = -1;	
						
				setState(55);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTENDS) {
					{
					setState(52);
					match(EXTENDS);
					setState(53);
					((CllistContext)_localctx).idSuperClass = match(ID);

									/*
					            	 * Cerchiamo la STentry di ID della superClass tramite
					            	 * la discesa di livelli della symbol table 
					            	 */
									STentry entry = symTable.get(0).get((((CllistContext)_localctx).idSuperClass!=null?((CllistContext)_localctx).idSuperClass.getText():null));
									
									if (entry == null) {
										System.out.println("Extended Class id "+(((CllistContext)_localctx).idSuperClass!=null?((CllistContext)_localctx).idSuperClass.getText():null)+" at line "+(((CllistContext)_localctx).idSuperClass!=null?((CllistContext)_localctx).idSuperClass.getLine():0)+" already declared");
						      			stErrors++;
									} else {
										classNode.setSuperEntry(entry);
										FOOLlib.setSuperType((((CllistContext)_localctx).idClass!=null?((CllistContext)_localctx).idClass.getText():null), (((CllistContext)_localctx).idSuperClass!=null?((CllistContext)_localctx).idSuperClass.getText():null));
									}
									
									try {
										classTypeNode = (ClassTypeNode) entry.getType().cloneNode();
						
										classNode.setFields(classTypeNode.getFields());
										classNode.setMethods(classTypeNode.getMethods());
										classNode.setSuperEntry(entry);
										classNode.setExtendId((((CllistContext)_localctx).idSuperClass!=null?((CllistContext)_localctx).idSuperClass.getText():null));
																
										superVirtualTable = classTable.get((((CllistContext)_localctx).idSuperClass!=null?((CllistContext)_localctx).idSuperClass.getText():null));
						 				Set<String> keys = superVirtualTable.keySet(); 
						 
						 				/*
						 				 * Eredita campi e metodi della virtual table del padre
						 				 */
						 				for (String k : keys) {
											virtualTable.put(k, superVirtualTable.get(k).cloneNode());
						 				}				
									} catch (CloneException e) {
										e.printStackTrace();
									}
								
									/*
									 * Nel caso in cui si eredita gli offset vengono aggiornati in base alla
									 * lunghezza di fields e methods di ClassTypeNode in STentry.
									 * -lunghezza-1 per i campi e lunghezza per i metodi
									 */
									methodOffset = classTypeNode.getMethods().size();
									fieldOffset = -classTypeNode.getFields().size();
								
					}
				}

												
					       	STentry entry = STentry.getEntry(nestingLevel, globalOffset); 
					       	entry.setType(classTypeNode);
					       	
							globalOffset = globalOffset - 1;
					       	
							HashMap<String,STentry> hm = symTable.get(nestingLevel);
					        
					        if ( hm.put((((CllistContext)_localctx).idClass!=null?((CllistContext)_localctx).idClass.getText():null),entry) != null  ) {
					      		System.out.println("Class id "+(((CllistContext)_localctx).idClass!=null?((CllistContext)_localctx).idClass.getText():null)+" at line "+(((CllistContext)_localctx).idClass!=null?((CllistContext)_localctx).idClass.getLine():0)+" already declared");
					      		stErrors++;
					     	} 
					        
					        classTable.put((((CllistContext)_localctx).idClass!=null?((CllistContext)_localctx).idClass.getText():null), virtualTable); 
					        
						   /*
					    	* Scope entry:
					    	* - aggiungo la virtual table al fronte della lista
					    	* - incremento il nesting level per entrare in un nuovo scope
					    	*/
					    	symTable.add(virtualTable);	                         	
					
					        nestingLevel = nestingLevel + 1;
						
				setState(58);
				match(LPAR);
				setState(74);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(59);
					((CllistContext)_localctx).idParameter = match(ID);
					setState(60);
					match(COLON);
					setState(61);
					((CllistContext)_localctx).t = type();

								   /* Ottimizzazione
						 	        * - Non è necessario controllare la ridefinizione (erronea)
						  	        *    del campo perchè è il primo.
							        * - Non sono ancora stati dichiarati campi
							        *   (e quindi non ce ne sono con lo stesso nome).
							        */
									FieldNode field = new FieldNode((((CllistContext)_localctx).idParameter!=null?((CllistContext)_localctx).idParameter.getText():null), ((CllistContext)_localctx).t.ast);
									
									STentry fieldEntry = null; 
									
									int entryOffset;

									/*
									 * Se nome di campo è già presente non lo considero errore: è overriding
									 * sostituisco nuova STentry alla vecchia preservando
									 * l'offset che era nella vecchia STentry
									 */ 
							        if (virtualTable.containsKey((((CllistContext)_localctx).idParameter!=null?((CllistContext)_localctx).idParameter.getText():null))) {            	
							        	STentry superEntry = virtualTable.get((((CllistContext)_localctx).idParameter!=null?((CllistContext)_localctx).idParameter.getText():null));
										int superOffset = superEntry.getOffset();
							        	
							        	/*
							        	 * Non consento l'overriding di un campo con un metodo 
							        	 */ 
							        	if (superEntry.isMethod()) {
											System.out.println("Field id "+ (((CllistContext)_localctx).idParameter!=null?((CllistContext)_localctx).idParameter.getText():null)+" at line "+(((CllistContext)_localctx).idParameter!=null?((CllistContext)_localctx).idParameter.getLine():0)+" is overriding a method");
											stErrors++;	    				        		
							        	} else {	            	

							            	fieldEntry = STentry.getEntry(nestingLevel, superOffset);
							            	
							            	/* Ottimizzazione */
							            	field.setOffset(superOffset);		            		  
							            	
							            	/*
							            	 * Per i campi aggiorno array allFields settando
							            	 * la posizione -offset-1 in base all'offset dell'STentry
							            	 * (Nel nostro layout l'offset del primo campo è -1)
							            	 */
											classTypeNode.addOverrideField((-superOffset-1), field);
											classNode.addOverrideField((-superOffset-1), field);
										}
									} else {
							     		classTypeNode.addField((-fieldOffset-1), field);
										classNode.addField((-fieldOffset-1), field);
									
										field.setOffset(fieldOffset);
							     		fieldEntry = STentry.getEntry(nestingLevel, fieldOffset--);
							     	}
							     	
							     	fieldEntry.setType(((CllistContext)_localctx).t.ast);
									virtualTable.put((((CllistContext)_localctx).idParameter!=null?((CllistContext)_localctx).idParameter.getText():null), fieldEntry);							
								
					setState(71);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(63);
						match(COMMA);
						setState(64);
						((CllistContext)_localctx).otherIdParameter = match(ID);
						setState(65);
						match(COLON);
						setState(66);
						((CllistContext)_localctx).ot = type();

											   /* Ottimizzazione:
						           				* Controlliamo che i nomi dei campi non siano già stati assegnati.
						           				*/
												if (classDeclarations.contains((((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null))) {
													System.out.println("Redefinition of field " + (((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null) + "at line" + (((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getLine():0));
													System.exit(0);
												}
												
												FieldNode otherField = new FieldNode((((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null), ((CllistContext)_localctx).ot.ast);
												
												STentry otherFieldEntry = null;
										        
												/*
										 		 * Se nome di campo è già presente non lo considero errore: è overriding
										 		 * sostituisco nuova STentry alla vecchia preservando
										 		 * l'offset che era nella vecchia STentry
										 		 */ 
										        if (virtualTable.containsKey((((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null))) {
										        	STentry superEntry = virtualTable.get((((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null));	
													
													/*
								        	 		 * Non consento l'overriding di un campo con un metodo 
								        	 		 */ 				        	
										        	if (superEntry.isMethod()) {
														System.out.println("Field id "+ (((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null)+" at line "+(((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getLine():0)+" is overriding a method");
														stErrors++;	    				        		
										        	} else {
										        		int superOffset = superEntry.getOffset();
										            	otherFieldEntry = STentry.getEntry(nestingLevel, superOffset);	
										            	
										            	/* Ottimizzazione */
														otherField.setOffset(superOffset);
							
													   /*
														* Per i campi aggiorno array allFields settando
														* la posizione -offset-1 in base all'offset dell'STentry
									            	 	* (Nel nostro layout l'offset del primo campo è -1)
									            	 	*/
										            	classTypeNode.addOverrideField((-superOffset-1), field);
														classNode.addOverrideField((-superOffset-1), field);            		
										        	}
										
										     	} else {
										     		classTypeNode.addField((-fieldOffset-1), otherField);
													classNode.addField((-fieldOffset-1), otherField);
													otherField.setOffset(fieldOffset);
										     		otherFieldEntry = STentry.getEntry(nestingLevel, fieldOffset--);
										     		
										     	}
										     	
										     	otherFieldEntry.setType(((CllistContext)_localctx).ot.ast);
												virtualTable.put((((CllistContext)_localctx).otherIdParameter!=null?((CllistContext)_localctx).otherIdParameter.getText():null), otherFieldEntry);
													            	            
										
						}
						}
						setState(73);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(76);
				match(RPAR);
				setState(77);
				match(CLPAR);
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==FUN) {
					{
					{
					setState(78);
					match(FUN);
					setState(79);
					((CllistContext)_localctx).idMethod = match(ID);
					setState(80);
					match(COLON);
					setState(81);
					((CllistContext)_localctx).methodType = type();

											/* Ottimizzazione:
					           				 * - Controllare che i nomi dei metodi non siano già stati assegnati.
					           				 */
											if (classDeclarations.contains((((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null))) {
												System.out.println("Redefinition of field " + (((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null) + "at line" + (((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getLine():0));
												System.exit(0);
											}
											
											ArrayList<Node> parameters = new ArrayList<>();
											ArrayList<Node> declarations = new ArrayList<Node>();
											
											//inserimento di ID nella symtable
											MethodNode methodNode = new MethodNode((((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null),((CllistContext)_localctx).methodType.ast);  
							       
											STentry methodEntry = null;
											
											/*
									 		 * Se nome di metodo è già presente non lo considero errore: è overriding
									 		 * sostituisco nuova STentry alla vecchia preservando
									 		 * l'offset che era nella vecchia STentry
									 		 */ 
									 		 if (virtualTable.containsKey((((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null))) {
											  STentry superEntry = virtualTable.get((((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null));
											 
											 /*
							        	 	  * Non consento l'overriding di un metodo con un campo
							        	 	  */ 		
											  if (!superEntry.isMethod()) {
											    System.out.println("Method id " + (((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null) + " at line " + (((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getLine():0) + " is overriding a field");
											    stErrors++;
											  } else {
											    int superOffset = superEntry.getOffset();

												methodNode.setOffset(superOffset);
											    methodEntry = STentry.getMethodEntry(nestingLevel, superOffset);  
										
							            	   /*
							            		* Per i metodi aggiorno array allFields settando
							            		* la posizione in base all'offset dell'STentry
							            	 	* (Nel nostro layout l'offset del primo metodo è 0)
							            	 	*/
												classTypeNode.addOverrideMethod(superOffset, methodNode);
												classNode.addOverrideMethod(superOffset, methodNode);
											  }
											} else {
											 	methodNode.setOffset(methodOffset);
											 	
												classTypeNode.addMethod(methodOffset, methodNode);
												classNode.addMethod(methodOffset, methodNode);
							
												methodEntry = STentry.getMethodEntry(nestingLevel, methodOffset);
							
												methodOffset = methodOffset + 1;
											}
											
											virtualTable.put((((CllistContext)_localctx).idMethod!=null?((CllistContext)_localctx).idMethod.getText():null), methodEntry);
							
											//creare una nuova hashmap per la symTable
									
										   /*
						    				* Scope entry:
						    				* - aggiungo una nuova tabella al fronte della lista
						    				* - incremento il nesting level per entrare in un nuovo scope
						    				*/
							        		HashMap<String,STentry> hmn = new HashMap<> ();
							        		symTable.add(hmn);
							        		nestingLevel++;

							        	
					setState(83);
					match(LPAR);
					setState(99);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(84);
						((CllistContext)_localctx).parameterId = match(ID);
						setState(85);
						match(COLON);
						setState(86);
						((CllistContext)_localctx).parameterType = hotype();
						 
													  int parameterOffset=1;
									                  parameters.add(((CllistContext)_localctx).parameterType.ast);
									                  
									                  ParNode methodPar = new ParNode((((CllistContext)_localctx).parameterId!=null?((CllistContext)_localctx).parameterId.getText():null),((CllistContext)_localctx).parameterType.ast); //creo nodo ParNode
									                  methodNode.addPar(methodPar);		
									                  
									                  STentry parameterEntry = STentry.getEntry(nestingLevel, parameterOffset);
									                  
													  /*
													   * Per i tipi funzionali l'offset vale doppio:
									                   * - indirizzo dell'AR della dichiarazione della funzione
									                   * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
									                   */ 
									                  if (((CllistContext)_localctx).parameterType.ast.getClass().equals(ArrowTypeNode.class)) {
						 								parameterOffset = parameterOffset + 2;
						 	  						  } else {
						 	  						  	parameterOffset = parameterOffset + 1;
						 	  						  }
									                  
									                  
									                  parameterEntry.setType(((CllistContext)_localctx).parameterType.ast);
									                  if ( hmn.put((((CllistContext)_localctx).parameterId!=null?((CllistContext)_localctx).parameterId.getText():null),parameterEntry) != null  ) {
									                   System.out.println("Parameter id "+(((CllistContext)_localctx).parameterId!=null?((CllistContext)_localctx).parameterId.getText():null)+" at line "+(((CllistContext)_localctx).parameterId!=null?((CllistContext)_localctx).parameterId.getLine():0)+" already declared");
									                   stErrors++;
									                  }   
									                  
								          			
						setState(96);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(88);
							match(COMMA);
							setState(89);
							((CllistContext)_localctx).otherParameterId = match(ID);
							setState(90);
							match(COLON);
							setState(91);
							((CllistContext)_localctx).otherParameterType = hotype();

									      							parameters.add(((CllistContext)_localctx).otherParameterType.ast);
							//		      							declarations.add(((CllistContext)_localctx).otherParameterType.ast);
										                  			ParNode otherMethodPar = new ParNode((((CllistContext)_localctx).otherParameterId!=null?((CllistContext)_localctx).otherParameterId.getText():null),((CllistContext)_localctx).otherParameterType.ast); //creo nodo ParNode
										                  			methodNode.addPar(otherMethodPar);		  		                  
										                  
																	STentry otherParameterEntry = STentry.getEntry(nestingLevel, parameterOffset);
																	
																	if (((CllistContext)_localctx).otherParameterType.ast.getClass().equals(ArrowTypeNode.class)) {
							             								parameterOffset = parameterOffset + 2;
							             	  						} else {
							             	  							parameterOffset = parameterOffset + 1;
							             	  						}
																	
																	otherParameterEntry.setType(((CllistContext)_localctx).otherParameterType.ast);
									
																	if ( hmn.put((((CllistContext)_localctx).otherParameterId!=null?((CllistContext)_localctx).otherParameterId.getText():null), otherParameterEntry) != null  ) {
																		System.out.println("Parameter id "+(((CllistContext)_localctx).otherParameterId!=null?((CllistContext)_localctx).otherParameterId.getText():null)+" at line "+(((CllistContext)_localctx).otherParameterId!=null?((CllistContext)_localctx).otherParameterId.getLine():0)+" already declared");
																		stErrors++;
										                  			}   
									      					
							}
							}
							setState(98);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(101);
					match(RPAR);
					setState(119);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(102);
						match(LET);

								      				ArrayList<Node> varList = new ArrayList<>();
								      				int varOffset = -2;
								      			
						setState(113); 
						_errHandler.sync(this);
						_la = _input.LA(1);
						do {
							{
							{
							setState(104);
							match(VAR);
							setState(105);
							((CllistContext)_localctx).varId = match(ID);
							setState(106);
							match(COLON);
							setState(107);
							((CllistContext)_localctx).varType = type();
							setState(108);
							match(ASS);
							setState(109);
							((CllistContext)_localctx).varExp = exp();
							setState(110);
							match(SEMIC);

									      					VarNode v = new VarNode((((CllistContext)_localctx).varId!=null?((CllistContext)_localctx).varId.getText():null),((CllistContext)_localctx).varType.ast,((CllistContext)_localctx).varExp.ast);  
															methodNode.addDec(v);
															HashMap<String,STentry> table = symTable.get(nestingLevel);
									     
															STentry varEntry = STentry.getEntry(nestingLevel, varOffset);
															
															varEntry.setType(((CllistContext)_localctx).varType.ast);
									     
															if ( table.put((((CllistContext)_localctx).varId!=null?((CllistContext)_localctx).varId.getText():null),entry) != null  ) {
									      						System.out.println("Var id "+(((CllistContext)_localctx).varId!=null?((CllistContext)_localctx).varId.getText():null)+" at line "+(((CllistContext)_localctx).varId!=null?((CllistContext)_localctx).varId.getLine():0)+" already declared");
																stErrors++;
															}
									     
															varOffset = varOffset - 1;  
									    				
							}
							}
							setState(115); 
							_errHandler.sync(this);
							_la = _input.LA(1);
						} while ( _la==VAR );
						setState(117);
						match(IN);
						}
					}

					setState(121);
					((CllistContext)_localctx).methodExp = exp();

								      			methodNode.addBody(((CllistContext)_localctx).methodExp.ast);
								      			              		
												methodEntry.setType(new ArrowTypeNode(parameters, ((CllistContext)_localctx).methodType.ast));
								      		
								      		    /*
						  	 					 * Scope exit:
						  	 					 * - Rimuovo la HashMap al nesting level corrente
						  	 					 * - Decremento il nesting level per ripristinarlo a quello dello scope più esterno rispetto a quello correnteF
						  	 					 */
								              	symTable.remove(nestingLevel);
										        nestingLevel = nestingLevel - 1;
								      		
					setState(123);
					match(SEMIC);
					}
					}
					setState(129);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(130);
				match(CRPAR);

				      			   /*
									* Scope exit:
									* - Rimuovo la HashMap al nesting level corrente
									* - Decremento il nesting level per ripristinarlo a quello dello scope più esterno rispetto a quello correnteF
									*/
				      				symTable.remove(nestingLevel);
				 					nestingLevel = nestingLevel - 1;
				      	     	
							  		_localctx.classList.add(classNode);
				  				
				}
				}
				setState(134); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CLASS );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclistContext extends ParserRuleContext {
		public ArrayList<Node> astlist;
		public Token i;
		public HotypeContext ht;
		public ExpContext e;
		public TypeContext t;
		public Token fid;
		public HotypeContext fty;
		public Token id;
		public HotypeContext ty;
		public DeclistContext d;
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<DeclistContext> declist() {
			return getRuleContexts(DeclistContext.class);
		}
		public DeclistContext declist(int i) {
			return getRuleContext(DeclistContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public DeclistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declist; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterDeclist(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitDeclist(this);
		}
	}

	public final DeclistContext declist() throws RecognitionException {
		DeclistContext _localctx = new DeclistContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((DeclistContext)_localctx).astlist =  new ArrayList<Node>() ;
				   int offset = -2; 
				   if (nestingLevel == 0) {
						offset = globalOffset;
				   }
				   
			setState(185); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(181);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case VAR:
					{
					setState(137);
					match(VAR);
					setState(138);
					((DeclistContext)_localctx).i = match(ID);
					setState(139);
					match(COLON);
					setState(140);
					((DeclistContext)_localctx).ht = hotype();
					setState(141);
					match(ASS);
					setState(142);
					((DeclistContext)_localctx).e = exp();
					VarNode v = new VarNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),((DeclistContext)_localctx).ht.ast,((DeclistContext)_localctx).e.ast);  
					             _localctx.astlist.add(v);                                 
					             HashMap<String,STentry> hm = symTable.get(nestingLevel);
					             
					             STentry entry = STentry.getEntry(nestingLevel, offset);
					             entry.setType(((DeclistContext)_localctx).ht.ast);
					             
					             if ( hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),entry) != null  ) {
					              System.out.println("Var id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared");
					              stErrors++;
					             }
					             
					 			/*
								 * Per i tipi funzionali l'offset vale doppio:
					             * - indirizzo dell'AR della dichiarazione della funzione
					             * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
					             */ 
					             if (v.getSymType() instanceof ArrowTypeNode) {
					             	offset = offset - 2;
					             } else {
					             	offset = offset - 1;
					             }  
					            
					}
					break;
				case FUN:
					{
					setState(145);
					match(FUN);
					setState(146);
					((DeclistContext)_localctx).i = match(ID);
					setState(147);
					match(COLON);
					setState(148);
					((DeclistContext)_localctx).t = type();
					//inserimento di ID nella symtable
					               FunNode f = new FunNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),((DeclistContext)_localctx).t.ast);      
					               _localctx.astlist.add(f);                              
					               HashMap<String,STentry> hm = symTable.get(nestingLevel);
					               ArrayList<Node> declarations = new ArrayList<Node>();
					               
					               STentry entry = STentry.getEntry(nestingLevel, offset); 

					               offset = offset - 2;
					             	
					               if (hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),entry) != null){
					                System.out.println("Fun id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared");
					                stErrors++;
					               }

								   /*
									* Scope entry:
									* - aggiungo una nuova tabella al fronte della lista
									* - incremento il nesting level per entrare in un nuovo scope
									*/
					                HashMap<String,STentry> hmn = new HashMap<String,STentry>();
					                symTable.add(hmn);
					                nestingLevel++;
					                
					setState(150);
					match(LPAR);
					int parameterOffset = 1;
					setState(167);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(152);
						((DeclistContext)_localctx).fid = match(ID);
						setState(153);
						match(COLON);
						setState(154);
						((DeclistContext)_localctx).fty = hotype();
						 
						                  declarations.add(((DeclistContext)_localctx).fty.ast);
						                  ParNode fpar = new ParNode((((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getText():null),((DeclistContext)_localctx).fty.ast); //creo nodo ParNode
						                  f.addPar(fpar);                                 //lo attacco al FunNode con addPar			  		                  

										 /*
									 	  * Per i tipi funzionali l'offset vale doppio:
						                  * - indirizzo dell'AR della dichiarazione della funzione
						                  * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
						             	  */ 
						                  if (((DeclistContext)_localctx).fty.ast instanceof ArrowTypeNode) {
						             			parameterOffset = parameterOffset + 1;
						             	  }
										                    
						                  STentry parameterEntry = STentry.getEntry(nestingLevel, parameterOffset++);
						                  
						                  parameterEntry.setType(((DeclistContext)_localctx).fty.ast);
						                  if ( hmn.put((((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getText():null),parameterEntry) != null  ) {
						                   System.out.println("Parameter id "+(((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getText():null)+" at line "+(((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getLine():0)+" already declared");
						                   stErrors++;
						                  }                  
						                
						setState(164);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(156);
							match(COMMA);
							setState(157);
							((DeclistContext)_localctx).id = match(ID);
							setState(158);
							match(COLON);
							setState(159);
							((DeclistContext)_localctx).ty = hotype();

							                    declarations.add(((DeclistContext)_localctx).ty.ast);
							                    ParNode par = new ParNode((((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getText():null),((DeclistContext)_localctx).ty.ast);
							                    f.addPar(par);
							                    
											   /*
										 	    * Per i tipi funzionali l'offset vale doppio:
							                    * - indirizzo dell'AR della dichiarazione della funzione
							                    * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
							             	    */ 
							                   	if (((DeclistContext)_localctx).ty.ast instanceof ArrowTypeNode) {
							             			parameterOffset = parameterOffset + 1;
							             	  	}
												
							                    STentry otherParameterEntry = STentry.getEntry(nestingLevel, parameterOffset++);
							                                        
							                  	otherParameterEntry.setType(((DeclistContext)_localctx).ty.ast);
							                    if ( hmn.put((((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getText():null),otherParameterEntry) != null  ) {
							                     System.out.println("Parameter id "+(((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getText():null)+" at line "+(((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getLine():0)+" already declared");
							                     stErrors++;
							                    }
							                    
							                    
							}
							}
							setState(166);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(169);
					match(RPAR);

					            	ArrowTypeNode symType = new ArrowTypeNode(declarations,((DeclistContext)_localctx).t.ast);
					            	entry.setType(symType);
					            	f.setSymType(symType);
					            
					setState(176);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(171);
						match(LET);
						setState(172);
						((DeclistContext)_localctx).d = declist();
						setState(173);
						match(IN);
						f.addDec(((DeclistContext)_localctx).d.astlist);
						}
					}

					setState(178);
					((DeclistContext)_localctx).e = exp();
					              	
						               f.addBody(((DeclistContext)_localctx).e.ast);
									  
									  /*
							  	 	   * Scope exit:
							  	 	   * - Rimuovo la HashMap al nesting level corrente
							  	 	   * - Decremento il nesting level per ripristinarlo a quello dello scope più esterno rispetto a quello correnteF
							  	 	   */	
						               symTable.remove(nestingLevel--);    
					              
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(183);
				match(SEMIC);
				}
				}
				setState(187); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==VAR || _la==FUN );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HotypeContext extends ParserRuleContext {
		public Node ast;
		public TypeContext t;
		public ArrowContext a;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ArrowContext arrow() {
			return getRuleContext(ArrowContext.class,0);
		}
		public HotypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hotype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterHotype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitHotype(this);
		}
	}

	public final HotypeContext hotype() throws RecognitionException {
		HotypeContext _localctx = new HotypeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_hotype);
		try {
			setState(195);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
			case BOOL:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(189);
				((HotypeContext)_localctx).t = type();
				((HotypeContext)_localctx).ast =  ((HotypeContext)_localctx).t.ast;
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(192);
				((HotypeContext)_localctx).a = arrow();
				((HotypeContext)_localctx).ast =  ((HotypeContext)_localctx).a.ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Node ast;
		public Token id;
		public TerminalNode INT() { return getToken(FOOLParser.INT, 0); }
		public TerminalNode BOOL() { return getToken(FOOLParser.BOOL, 0); }
		public TerminalNode ID() { return getToken(FOOLParser.ID, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type);
		try {
			setState(203);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(197);
				match(INT);
				((TypeContext)_localctx).ast =  new IntTypeNode();
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(199);
				match(BOOL);
				((TypeContext)_localctx).ast =  new BoolTypeNode();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 3);
				{
				setState(201);
				((TypeContext)_localctx).id = match(ID);
				((TypeContext)_localctx).ast =  new RefTypeNode((((TypeContext)_localctx).id!=null?((TypeContext)_localctx).id.getText():null));
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrowContext extends ParserRuleContext {
		public Node ast;
		public HotypeContext h;
		public HotypeContext hot;
		public TypeContext t;
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public TerminalNode ARROW() { return getToken(FOOLParser.ARROW, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public ArrowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrow; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterArrow(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitArrow(this);
		}
	}

	public final ArrowContext arrow() throws RecognitionException {
		ArrowContext _localctx = new ArrowContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_arrow);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205);
			match(LPAR);
			ArrayList arguments = new ArrayList<Node>();
			setState(218);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAR) | (1L << INT) | (1L << BOOL) | (1L << ID))) != 0)) {
				{
				setState(207);
				((ArrowContext)_localctx).h = hotype();
				arguments.add(((ArrowContext)_localctx).h.ast);
				setState(215);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(209);
					match(COMMA);
					setState(210);
					((ArrowContext)_localctx).hot = hotype();
					arguments.add(((ArrowContext)_localctx).hot.ast);
					}
					}
					setState(217);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(220);
			match(RPAR);
			setState(221);
			match(ARROW);
			setState(222);
			((ArrowContext)_localctx).t = type();
			((ArrowContext)_localctx).ast =  new ArrowTypeNode(arguments, ((ArrowContext)_localctx).t.ast);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public Node ast;
		public TermContext f;
		public TermContext l;
		public TermContext t;
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(FOOLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(FOOLParser.PLUS, i);
		}
		public List<TerminalNode> MINUS() { return getTokens(FOOLParser.MINUS); }
		public TerminalNode MINUS(int i) {
			return getToken(FOOLParser.MINUS, i);
		}
		public List<TerminalNode> OR() { return getTokens(FOOLParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(FOOLParser.OR, i);
		}
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitExp(this);
		}
	}

	public final ExpContext exp() throws RecognitionException {
		ExpContext _localctx = new ExpContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225);
			((ExpContext)_localctx).f = term();
			((ExpContext)_localctx).ast =  ((ExpContext)_localctx).f.ast;
			setState(241);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OR) | (1L << MINUS) | (1L << PLUS))) != 0)) {
				{
				setState(239);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS:
					{
					setState(227);
					match(PLUS);
					setState(228);
					((ExpContext)_localctx).l = term();
					((ExpContext)_localctx).ast =  new PlusNode (_localctx.ast,((ExpContext)_localctx).l.ast);
					}
					break;
				case MINUS:
					{
					setState(231);
					match(MINUS);
					setState(232);
					((ExpContext)_localctx).t = term();
					((ExpContext)_localctx).ast =  new MinusNode(_localctx.ast,((ExpContext)_localctx).t.ast);
					}
					break;
				case OR:
					{
					setState(235);
					match(OR);
					setState(236);
					((ExpContext)_localctx).t = term();
					((ExpContext)_localctx).ast =  new OrNode (_localctx.ast,((ExpContext)_localctx).t.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(243);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public Node ast;
		public FactorContext f;
		public FactorContext l;
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<TerminalNode> TIMES() { return getTokens(FOOLParser.TIMES); }
		public TerminalNode TIMES(int i) {
			return getToken(FOOLParser.TIMES, i);
		}
		public List<TerminalNode> DIV() { return getTokens(FOOLParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(FOOLParser.DIV, i);
		}
		public List<TerminalNode> AND() { return getTokens(FOOLParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(FOOLParser.AND, i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitTerm(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(244);
			((TermContext)_localctx).f = factor();
			((TermContext)_localctx).ast =  ((TermContext)_localctx).f.ast;
			setState(260);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AND) | (1L << DIV) | (1L << TIMES))) != 0)) {
				{
				setState(258);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case TIMES:
					{
					setState(246);
					match(TIMES);
					setState(247);
					((TermContext)_localctx).l = factor();
					((TermContext)_localctx).ast =  new TimesNode (_localctx.ast,((TermContext)_localctx).l.ast);
					}
					break;
				case DIV:
					{
					setState(250);
					match(DIV);
					setState(251);
					((TermContext)_localctx).f = factor();
					((TermContext)_localctx).ast =  new DivNode(_localctx.ast, ((TermContext)_localctx).f.ast);
					}
					break;
				case AND:
					{
					setState(254);
					match(AND);
					setState(255);
					((TermContext)_localctx).f = factor();
					((TermContext)_localctx).ast =  new AndNode(_localctx.ast, ((TermContext)_localctx).f.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(262);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public Node ast;
		public ValueContext f;
		public ValueContext l;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<TerminalNode> EQ() { return getTokens(FOOLParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(FOOLParser.EQ, i);
		}
		public List<TerminalNode> GE() { return getTokens(FOOLParser.GE); }
		public TerminalNode GE(int i) {
			return getToken(FOOLParser.GE, i);
		}
		public List<TerminalNode> LE() { return getTokens(FOOLParser.LE); }
		public TerminalNode LE(int i) {
			return getToken(FOOLParser.LE, i);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitFactor(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_factor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(263);
			((FactorContext)_localctx).f = value();
			((FactorContext)_localctx).ast =  ((FactorContext)_localctx).f.ast;
			setState(279);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << GE) | (1L << LE))) != 0)) {
				{
				setState(277);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EQ:
					{
					setState(265);
					match(EQ);
					setState(266);
					((FactorContext)_localctx).l = value();
					((FactorContext)_localctx).ast =  new EqualNode (_localctx.ast,((FactorContext)_localctx).l.ast);
					}
					break;
				case GE:
					{
					setState(269);
					match(GE);
					setState(270);
					((FactorContext)_localctx).l = value();
					((FactorContext)_localctx).ast =  new GreaterEqualNode(_localctx.ast, ((FactorContext)_localctx).l.ast);
					}
					break;
				case LE:
					{
					setState(273);
					match(LE);
					setState(274);
					((FactorContext)_localctx).l = value();
					((FactorContext)_localctx).ast =  new LesserEqualNode(_localctx.ast, ((FactorContext)_localctx).l.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(281);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public Node ast;
		public Token n;
		public ExpContext e;
		public Token newId;
		public ExpContext ex;
		public ExpContext x;
		public ExpContext y;
		public ExpContext z;
		public Token i;
		public ExpContext a;
		public Token id2;
		public TerminalNode INTEGER() { return getToken(FOOLParser.INTEGER, 0); }
		public TerminalNode TRUE() { return getToken(FOOLParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(FOOLParser.FALSE, 0); }
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode NULL() { return getToken(FOOLParser.NULL, 0); }
		public TerminalNode NEW() { return getToken(FOOLParser.NEW, 0); }
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public TerminalNode IF() { return getToken(FOOLParser.IF, 0); }
		public TerminalNode THEN() { return getToken(FOOLParser.THEN, 0); }
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public TerminalNode ELSE() { return getToken(FOOLParser.ELSE, 0); }
		public TerminalNode PRINT() { return getToken(FOOLParser.PRINT, 0); }
		public TerminalNode NOT() { return getToken(FOOLParser.NOT, 0); }
		public TerminalNode DOT() { return getToken(FOOLParser.DOT, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FOOLListener ) ((FOOLListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_value);
		int _la;
		try {
			setState(378);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
				enterOuterAlt(_localctx, 1);
				{
				setState(282);
				((ValueContext)_localctx).n = match(INTEGER);
				((ValueContext)_localctx).ast =  new IntNode(Integer.parseInt((((ValueContext)_localctx).n!=null?((ValueContext)_localctx).n.getText():null)));
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 2);
				{
				setState(284);
				match(TRUE);
				((ValueContext)_localctx).ast =  new BoolNode(true);
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 3);
				{
				setState(286);
				match(FALSE);
				((ValueContext)_localctx).ast =  new BoolNode(false);
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 4);
				{
				setState(288);
				match(LPAR);
				setState(289);
				((ValueContext)_localctx).e = exp();
				setState(290);
				match(RPAR);
				((ValueContext)_localctx).ast =  ((ValueContext)_localctx).e.ast;
				}
				break;
			case NULL:
				enterOuterAlt(_localctx, 5);
				{
				setState(293);
				match(NULL);
				((ValueContext)_localctx).ast =  new EmptyNode();
				}
				break;
			case NEW:
				enterOuterAlt(_localctx, 6);
				{
				setState(295);
				match(NEW);
				setState(296);
				((ValueContext)_localctx).newId = match(ID);
				setState(297);
				match(LPAR);

						boolean isIdInClassTable = this.classTable.containsKey((((ValueContext)_localctx).newId!=null?((ValueContext)_localctx).newId.getText():null));
						
					   /*
				        * Recuperiamo la STentry dell'ID della classe
				        * accedendo direttamente al livello 0 (ambiente globale)
				        * in cui sono memorizzate le dichiarazioni delle classi
				        */
				        STentry entry = symTable.get(0).get((((ValueContext)_localctx).newId!=null?((ValueContext)_localctx).newId.getText():null));
				    	if (entry == null || !isIdInClassTable) {
				    		System.out.println((((ValueContext)_localctx).newId!=null?((ValueContext)_localctx).newId.getText():null)+" at line "+(((ValueContext)_localctx).newId!=null?((ValueContext)_localctx).newId.getLine():0)+" not declared");
				            stErrors++;              
				    	}			
				    	
				    	List<Node> parameters = new ArrayList<>();
				    	
				    
				setState(310);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << INTEGER) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << ID))) != 0)) {
					{
					setState(299);
					((ValueContext)_localctx).e = exp();

					    	parameters.add(((ValueContext)_localctx).e.ast);
					    
					setState(307);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(301);
						match(COMMA);
						setState(302);
						((ValueContext)_localctx).ex = exp();

						    	parameters.add(((ValueContext)_localctx).ex.ast);
						    
						}
						}
						setState(309);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(312);
				match(RPAR);

				    	((ValueContext)_localctx).ast =  new NewNode((((ValueContext)_localctx).newId!=null?((ValueContext)_localctx).newId.getText():null), entry, parameters);
				    
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 7);
				{
				setState(314);
				match(IF);
				setState(315);
				((ValueContext)_localctx).x = exp();
				setState(316);
				match(THEN);
				setState(317);
				match(CLPAR);
				setState(318);
				((ValueContext)_localctx).y = exp();
				setState(319);
				match(CRPAR);
				setState(320);
				match(ELSE);
				setState(321);
				match(CLPAR);
				setState(322);
				((ValueContext)_localctx).z = exp();
				setState(323);
				match(CRPAR);
				((ValueContext)_localctx).ast =  new IfNode(((ValueContext)_localctx).x.ast,((ValueContext)_localctx).y.ast,((ValueContext)_localctx).z.ast);
				}
				break;
			case PRINT:
				enterOuterAlt(_localctx, 8);
				{
				setState(326);
				match(PRINT);
				setState(327);
				match(LPAR);
				setState(328);
				((ValueContext)_localctx).e = exp();
				setState(329);
				match(RPAR);
				((ValueContext)_localctx).ast =  new PrintNode(((ValueContext)_localctx).e.ast);
				}
				break;
			case NOT:
				enterOuterAlt(_localctx, 9);
				{
				setState(332);
				match(NOT);
				setState(333);
				match(LPAR);
				setState(334);
				((ValueContext)_localctx).e = exp();
				setState(335);
				match(RPAR);
				((ValueContext)_localctx).ast =  new NotNode(((ValueContext)_localctx).e.ast);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 10);
				{
				setState(338);
				((ValueContext)_localctx).i = match(ID);

							
				           /*
				            * Cerchiamo la STentry di ID1 tramite la discesa di livelli
				            * della symbol table 
				            */
				           int j=nestingLevel;
				           
				           STentry entry=null; 
				  
				           while (j>=0 && entry==null)
				             entry=(symTable.get(j--)).get((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null));
				             
				           if (entry==null) {
				             	System.out.println("Id "+(((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null)+" at line "+(((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getLine():0)+" not declared");
				             	stErrors++;
				           }	               
						   ((ValueContext)_localctx).ast =  new IdNode((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null),entry,nestingLevel);
					   
				setState(376);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LPAR:
					{
					setState(340);
					match(LPAR);
					ArrayList<Node> arglist = new ArrayList<Node>();
					setState(353);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << INTEGER) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << ID))) != 0)) {
						{
						setState(342);
						((ValueContext)_localctx).a = exp();
						arglist.add(((ValueContext)_localctx).a.ast);
						setState(350);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(344);
							match(COMMA);
							setState(345);
							((ValueContext)_localctx).a = exp();
							arglist.add(((ValueContext)_localctx).a.ast);
							}
							}
							setState(352);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(355);
					match(RPAR);
					((ValueContext)_localctx).ast =  new CallNode((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null), entry, arglist, nestingLevel);
					}
					break;
				case DOT:
					{
					setState(357);
					match(DOT);
					setState(358);
					((ValueContext)_localctx).id2 = match(ID);
					setState(359);
					match(LPAR);

						   							List<Node> fields = new ArrayList<>();
						   						  
					setState(372);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << INTEGER) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << ID))) != 0)) {
						{
						setState(361);
						((ValueContext)_localctx).e = exp();
						fields.add(((ValueContext)_localctx).e.ast);
						setState(369);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(363);
							match(COMMA);
							setState(364);
							((ValueContext)_localctx).e = exp();
							fields.add(((ValueContext)_localctx).e.ast);
							}
							}
							setState(371);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}


												if (entry.getType() instanceof RefTypeNode) {
											   		RefTypeNode typeObject = (RefTypeNode) entry.getType();
											   		String idObject = typeObject.getId();
											   		HashMap<String, STentry> virtualTable = classTable.get(idObject);
											   		
											   		/*
											   		 * Recupero l'STentry di id2 in VirtualTable
					  						   		 */						   		
											   		STentry methodEntry = virtualTable.get((((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getText():null));
											   		if (methodEntry == null) {
														System.out.println("Method "+(((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getText():null)+" at line "+(((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getLine():0)+" not declared");
										             	stErrors++;               		
											   		}
											   		
											   		((ValueContext)_localctx).ast =  new ClassCallNode(idObject, (((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getText():null), entry, methodEntry, fields, nestingLevel);								
												} else {
													System.out.println((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null) + " is not an object. Error at line " + (((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getLine():0) );
										            stErrors++;               	
												}
											   
					setState(375);
					match(RPAR);
					}
					break;
				case COMMA:
				case SEMIC:
				case EQ:
				case OR:
				case AND:
				case GE:
				case LE:
				case DIV:
				case MINUS:
				case PLUS:
				case TIMES:
				case RPAR:
				case CRPAR:
				case THEN:
					break;
				default:
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+\u017f\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\"\n\2\3\2\3\2\3\2"+
		"\5\2\'\n\2\3\2\3\2\3\2\3\2\5\2-\n\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\5\3:\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7"+
		"\3H\n\3\f\3\16\3K\13\3\5\3M\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3a\n\3\f\3\16\3d\13\3\5\3f\n\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\6\3t\n\3\r\3\16\3u\3\3"+
		"\3\3\5\3z\n\3\3\3\3\3\3\3\3\3\7\3\u0080\n\3\f\3\16\3\u0083\13\3\3\3\3"+
		"\3\6\3\u0087\n\3\r\3\16\3\u0088\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4"+
		"\u00a5\n\4\f\4\16\4\u00a8\13\4\5\4\u00aa\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\5\4\u00b3\n\4\3\4\3\4\3\4\5\4\u00b8\n\4\3\4\3\4\6\4\u00bc\n\4\r\4\16"+
		"\4\u00bd\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u00c6\n\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\5\6\u00ce\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u00d8\n\7\f\7\16\7"+
		"\u00db\13\7\5\7\u00dd\n\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\b\u00f2\n\b\f\b\16\b\u00f5\13\b\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u0105\n\t\f\t"+
		"\16\t\u0108\13\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\7\n\u0118\n\n\f\n\16\n\u011b\13\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\7\13\u0134\n\13\f\13\16\13\u0137\13\13\5\13\u0139\n\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u015f\n\13\f\13\16\13\u0162\13"+
		"\13\5\13\u0164\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\7\13\u0172\n\13\f\13\16\13\u0175\13\13\5\13\u0177\n\13\3\13"+
		"\3\13\5\13\u017b\n\13\5\13\u017d\n\13\3\13\2\2\f\2\4\6\b\n\f\16\20\22"+
		"\24\2\2\2\u01a4\2\26\3\2\2\2\4\62\3\2\2\2\6\u008a\3\2\2\2\b\u00c5\3\2"+
		"\2\2\n\u00cd\3\2\2\2\f\u00cf\3\2\2\2\16\u00e3\3\2\2\2\20\u00f6\3\2\2\2"+
		"\22\u0109\3\2\2\2\24\u017c\3\2\2\2\26,\b\2\1\2\27\30\5\16\b\2\30\31\b"+
		"\2\1\2\31-\3\2\2\2\32\33\7\34\2\2\33&\b\2\1\2\34\35\5\4\3\2\35!\b\2\1"+
		"\2\36\37\5\6\4\2\37 \b\2\1\2 \"\3\2\2\2!\36\3\2\2\2!\"\3\2\2\2\"\'\3\2"+
		"\2\2#$\5\6\4\2$%\b\2\1\2%\'\3\2\2\2&\34\3\2\2\2&#\3\2\2\2\'(\3\2\2\2("+
		")\7\35\2\2)*\5\16\b\2*+\b\2\1\2+-\3\2\2\2,\27\3\2\2\2,\32\3\2\2\2-.\3"+
		"\2\2\2./\b\2\1\2/\60\7\6\2\2\60\61\7\2\2\3\61\3\3\2\2\2\62\u0086\b\3\1"+
		"\2\63\64\7 \2\2\64\65\7(\2\2\659\b\3\1\2\66\67\7!\2\2\678\7(\2\28:\b\3"+
		"\1\29\66\3\2\2\29:\3\2\2\2:;\3\2\2\2;<\b\3\1\2<L\7\24\2\2=>\7(\2\2>?\7"+
		"\3\2\2?@\5\n\6\2@I\b\3\1\2AB\7\4\2\2BC\7(\2\2CD\7\3\2\2DE\5\n\6\2EF\b"+
		"\3\1\2FH\3\2\2\2GA\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2JM\3\2\2\2KI\3"+
		"\2\2\2L=\3\2\2\2LM\3\2\2\2MN\3\2\2\2NO\7\25\2\2O\u0081\7\26\2\2PQ\7\37"+
		"\2\2QR\7(\2\2RS\7\3\2\2ST\5\n\6\2TU\b\3\1\2Ue\7\24\2\2VW\7(\2\2WX\7\3"+
		"\2\2XY\5\b\5\2Yb\b\3\1\2Z[\7\4\2\2[\\\7(\2\2\\]\7\3\2\2]^\5\b\5\2^_\b"+
		"\3\1\2_a\3\2\2\2`Z\3\2\2\2ad\3\2\2\2b`\3\2\2\2bc\3\2\2\2cf\3\2\2\2db\3"+
		"\2\2\2eV\3\2\2\2ef\3\2\2\2fg\3\2\2\2gy\7\25\2\2hi\7\34\2\2is\b\3\1\2j"+
		"k\7\36\2\2kl\7(\2\2lm\7\3\2\2mn\5\n\6\2no\7\5\2\2op\5\16\b\2pq\7\6\2\2"+
		"qr\b\3\1\2rt\3\2\2\2sj\3\2\2\2tu\3\2\2\2us\3\2\2\2uv\3\2\2\2vw\3\2\2\2"+
		"wx\7\35\2\2xz\3\2\2\2yh\3\2\2\2yz\3\2\2\2z{\3\2\2\2{|\5\16\b\2|}\b\3\1"+
		"\2}~\7\6\2\2~\u0080\3\2\2\2\177P\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177"+
		"\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0084\3\2\2\2\u0083\u0081\3\2\2\2\u0084"+
		"\u0085\7\27\2\2\u0085\u0087\b\3\1\2\u0086\63\3\2\2\2\u0087\u0088\3\2\2"+
		"\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089\5\3\2\2\2\u008a\u00bb"+
		"\b\4\1\2\u008b\u008c\7\36\2\2\u008c\u008d\7(\2\2\u008d\u008e\7\3\2\2\u008e"+
		"\u008f\5\b\5\2\u008f\u0090\7\5\2\2\u0090\u0091\5\16\b\2\u0091\u0092\b"+
		"\4\1\2\u0092\u00b8\3\2\2\2\u0093\u0094\7\37\2\2\u0094\u0095\7(\2\2\u0095"+
		"\u0096\7\3\2\2\u0096\u0097\5\n\6\2\u0097\u0098\b\4\1\2\u0098\u0099\7\24"+
		"\2\2\u0099\u00a9\b\4\1\2\u009a\u009b\7(\2\2\u009b\u009c\7\3\2\2\u009c"+
		"\u009d\5\b\5\2\u009d\u00a6\b\4\1\2\u009e\u009f\7\4\2\2\u009f\u00a0\7("+
		"\2\2\u00a0\u00a1\7\3\2\2\u00a1\u00a2\5\b\5\2\u00a2\u00a3\b\4\1\2\u00a3"+
		"\u00a5\3\2\2\2\u00a4\u009e\3\2\2\2\u00a5\u00a8\3\2\2\2\u00a6\u00a4\3\2"+
		"\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00aa\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a9"+
		"\u009a\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\7\25"+
		"\2\2\u00ac\u00b2\b\4\1\2\u00ad\u00ae\7\34\2\2\u00ae\u00af\5\6\4\2\u00af"+
		"\u00b0\7\35\2\2\u00b0\u00b1\b\4\1\2\u00b1\u00b3\3\2\2\2\u00b2\u00ad\3"+
		"\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b5\5\16\b\2\u00b5"+
		"\u00b6\b\4\1\2\u00b6\u00b8\3\2\2\2\u00b7\u008b\3\2\2\2\u00b7\u0093\3\2"+
		"\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00ba\7\6\2\2\u00ba\u00bc\3\2\2\2\u00bb"+
		"\u00b7\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00bb\3\2\2\2\u00bd\u00be\3\2"+
		"\2\2\u00be\7\3\2\2\2\u00bf\u00c0\5\n\6\2\u00c0\u00c1\b\5\1\2\u00c1\u00c6"+
		"\3\2\2\2\u00c2\u00c3\5\f\7\2\u00c3\u00c4\b\5\1\2\u00c4\u00c6\3\2\2\2\u00c5"+
		"\u00bf\3\2\2\2\u00c5\u00c2\3\2\2\2\u00c6\t\3\2\2\2\u00c7\u00c8\7$\2\2"+
		"\u00c8\u00ce\b\6\1\2\u00c9\u00ca\7%\2\2\u00ca\u00ce\b\6\1\2\u00cb\u00cc"+
		"\7(\2\2\u00cc\u00ce\b\6\1\2\u00cd\u00c7\3\2\2\2\u00cd\u00c9\3\2\2\2\u00cd"+
		"\u00cb\3\2\2\2\u00ce\13\3\2\2\2\u00cf\u00d0\7\24\2\2\u00d0\u00dc\b\7\1"+
		"\2\u00d1\u00d2\5\b\5\2\u00d2\u00d9\b\7\1\2\u00d3\u00d4\7\4\2\2\u00d4\u00d5"+
		"\5\b\5\2\u00d5\u00d6\b\7\1\2\u00d6\u00d8\3\2\2\2\u00d7\u00d3\3\2\2\2\u00d8"+
		"\u00db\3\2\2\2\u00d9\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00dd\3\2"+
		"\2\2\u00db\u00d9\3\2\2\2\u00dc\u00d1\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd"+
		"\u00de\3\2\2\2\u00de\u00df\7\25\2\2\u00df\u00e0\7&\2\2\u00e0\u00e1\5\n"+
		"\6\2\u00e1\u00e2\b\7\1\2\u00e2\r\3\2\2\2\u00e3\u00e4\5\20\t\2\u00e4\u00f3"+
		"\b\b\1\2\u00e5\u00e6\7\17\2\2\u00e6\u00e7\5\20\t\2\u00e7\u00e8\b\b\1\2"+
		"\u00e8\u00f2\3\2\2\2\u00e9\u00ea\7\16\2\2\u00ea\u00eb\5\20\t\2\u00eb\u00ec"+
		"\b\b\1\2\u00ec\u00f2\3\2\2\2\u00ed\u00ee\7\b\2\2\u00ee\u00ef\5\20\t\2"+
		"\u00ef\u00f0\b\b\1\2\u00f0\u00f2\3\2\2\2\u00f1\u00e5\3\2\2\2\u00f1\u00e9"+
		"\3\2\2\2\u00f1\u00ed\3\2\2\2\u00f2\u00f5\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f3"+
		"\u00f4\3\2\2\2\u00f4\17\3\2\2\2\u00f5\u00f3\3\2\2\2\u00f6\u00f7\5\22\n"+
		"\2\u00f7\u0106\b\t\1\2\u00f8\u00f9\7\20\2\2\u00f9\u00fa\5\22\n\2\u00fa"+
		"\u00fb\b\t\1\2\u00fb\u0105\3\2\2\2\u00fc\u00fd\7\r\2\2\u00fd\u00fe\5\22"+
		"\n\2\u00fe\u00ff\b\t\1\2\u00ff\u0105\3\2\2\2\u0100\u0101\7\t\2\2\u0101"+
		"\u0102\5\22\n\2\u0102\u0103\b\t\1\2\u0103\u0105\3\2\2\2\u0104\u00f8\3"+
		"\2\2\2\u0104\u00fc\3\2\2\2\u0104\u0100\3\2\2\2\u0105\u0108\3\2\2\2\u0106"+
		"\u0104\3\2\2\2\u0106\u0107\3\2\2\2\u0107\21\3\2\2\2\u0108\u0106\3\2\2"+
		"\2\u0109\u010a\5\24\13\2\u010a\u0119\b\n\1\2\u010b\u010c\7\7\2\2\u010c"+
		"\u010d\5\24\13\2\u010d\u010e\b\n\1\2\u010e\u0118\3\2\2\2\u010f\u0110\7"+
		"\13\2\2\u0110\u0111\5\24\13\2\u0111\u0112\b\n\1\2\u0112\u0118\3\2\2\2"+
		"\u0113\u0114\7\f\2\2\u0114\u0115\5\24\13\2\u0115\u0116\b\n\1\2\u0116\u0118"+
		"\3\2\2\2\u0117\u010b\3\2\2\2\u0117\u010f\3\2\2\2\u0117\u0113\3\2\2\2\u0118"+
		"\u011b\3\2\2\2\u0119\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011a\23\3\2\2"+
		"\2\u011b\u0119\3\2\2\2\u011c\u011d\7\21\2\2\u011d\u017d\b\13\1\2\u011e"+
		"\u011f\7\22\2\2\u011f\u017d\b\13\1\2\u0120\u0121\7\23\2\2\u0121\u017d"+
		"\b\13\1\2\u0122\u0123\7\24\2\2\u0123\u0124\5\16\b\2\u0124\u0125\7\25\2"+
		"\2\u0125\u0126\b\13\1\2\u0126\u017d\3\2\2\2\u0127\u0128\7#\2\2\u0128\u017d"+
		"\b\13\1\2\u0129\u012a\7\"\2\2\u012a\u012b\7(\2\2\u012b\u012c\7\24\2\2"+
		"\u012c\u0138\b\13\1\2\u012d\u012e\5\16\b\2\u012e\u0135\b\13\1\2\u012f"+
		"\u0130\7\4\2\2\u0130\u0131\5\16\b\2\u0131\u0132\b\13\1\2\u0132\u0134\3"+
		"\2\2\2\u0133\u012f\3\2\2\2\u0134\u0137\3\2\2\2\u0135\u0133\3\2\2\2\u0135"+
		"\u0136\3\2\2\2\u0136\u0139\3\2\2\2\u0137\u0135\3\2\2\2\u0138\u012d\3\2"+
		"\2\2\u0138\u0139\3\2\2\2\u0139\u013a\3\2\2\2\u013a\u013b\7\25\2\2\u013b"+
		"\u017d\b\13\1\2\u013c\u013d\7\30\2\2\u013d\u013e\5\16\b\2\u013e\u013f"+
		"\7\31\2\2\u013f\u0140\7\26\2\2\u0140\u0141\5\16\b\2\u0141\u0142\7\27\2"+
		"\2\u0142\u0143\7\32\2\2\u0143\u0144\7\26\2\2\u0144\u0145\5\16\b\2\u0145"+
		"\u0146\7\27\2\2\u0146\u0147\b\13\1\2\u0147\u017d\3\2\2\2\u0148\u0149\7"+
		"\33\2\2\u0149\u014a\7\24\2\2\u014a\u014b\5\16\b\2\u014b\u014c\7\25\2\2"+
		"\u014c\u014d\b\13\1\2\u014d\u017d\3\2\2\2\u014e\u014f\7\n\2\2\u014f\u0150"+
		"\7\24\2\2\u0150\u0151\5\16\b\2\u0151\u0152\7\25\2\2\u0152\u0153\b\13\1"+
		"\2\u0153\u017d\3\2\2\2\u0154\u0155\7(\2\2\u0155\u017a\b\13\1\2\u0156\u0157"+
		"\7\24\2\2\u0157\u0163\b\13\1\2\u0158\u0159\5\16\b\2\u0159\u0160\b\13\1"+
		"\2\u015a\u015b\7\4\2\2\u015b\u015c\5\16\b\2\u015c\u015d\b\13\1\2\u015d"+
		"\u015f\3\2\2\2\u015e\u015a\3\2\2\2\u015f\u0162\3\2\2\2\u0160\u015e\3\2"+
		"\2\2\u0160\u0161\3\2\2\2\u0161\u0164\3\2\2\2\u0162\u0160\3\2\2\2\u0163"+
		"\u0158\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0165\3\2\2\2\u0165\u0166\7\25"+
		"\2\2\u0166\u017b\b\13\1\2\u0167\u0168\7\'\2\2\u0168\u0169\7(\2\2\u0169"+
		"\u016a\7\24\2\2\u016a\u0176\b\13\1\2\u016b\u016c\5\16\b\2\u016c\u0173"+
		"\b\13\1\2\u016d\u016e\7\4\2\2\u016e\u016f\5\16\b\2\u016f\u0170\b\13\1"+
		"\2\u0170\u0172\3\2\2\2\u0171\u016d\3\2\2\2\u0172\u0175\3\2\2\2\u0173\u0171"+
		"\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0177\3\2\2\2\u0175\u0173\3\2\2\2\u0176"+
		"\u016b\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0178\3\2\2\2\u0178\u0179\b\13"+
		"\1\2\u0179\u017b\7\25\2\2\u017a\u0156\3\2\2\2\u017a\u0167\3\2\2\2\u017a"+
		"\u017b\3\2\2\2\u017b\u017d\3\2\2\2\u017c\u011c\3\2\2\2\u017c\u011e\3\2"+
		"\2\2\u017c\u0120\3\2\2\2\u017c\u0122\3\2\2\2\u017c\u0127\3\2\2\2\u017c"+
		"\u0129\3\2\2\2\u017c\u013c\3\2\2\2\u017c\u0148\3\2\2\2\u017c\u014e\3\2"+
		"\2\2\u017c\u0154\3\2\2\2\u017d\25\3\2\2\2%!&,9ILbeuy\u0081\u0088\u00a6"+
		"\u00a9\u00b2\u00b7\u00bd\u00c5\u00cd\u00d9\u00dc\u00f1\u00f3\u0104\u0106"+
		"\u0117\u0119\u0135\u0138\u0160\u0163\u0173\u0176\u017a\u017c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}