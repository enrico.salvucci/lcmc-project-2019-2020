package lib;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import ast.*;

public class FOOLlib {

	public static final int MEMSIZE = 10000;
	public static List<List<String>> dispatchTables = new ArrayList<>();
	/*
	 * Mappa id classe con quello della sua classe super
	 */
	private static Map<String, String> superType = new HashMap<>();

	public static int typeErrors = 0;

	/*
	 * valuta se il tipo "a" è <= al tipo "b", dove "a" e "b" sono tipi di base: int o bool
	 */
	public static boolean isSubtype(Node a, Node b) {
		return a.getClass().equals(b.getClass())
				|| ((a instanceof BoolTypeNode) && (b instanceof IntTypeNode)
				|| isArrowTypeNode(a, b))
				|| isSuperType(b, a)
				|| isEmptyNodeSubtypeOfRefTypeNode(a, b);
	}

	private static int labCount = 0;
	private static int metLabCount = 0;

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	private static int funlabCount = 0;

	public static String freshFunLabel() {
		return "function" + (funlabCount++);
	}

	public static String freshMetLabel() {
		return "method"+(metLabCount++);
	}
	
	public static void setSuperType(String childClass, String superClass) {
		superType.put(childClass, superClass);
	}
	
	private static String funCode = "";

	public static void putCode(String c) {
		funCode += "\n" + c; // aggiunge una linea vuota di separazione prima di funzione
	}

	public static String getCode() {
		return funCode;
	}
	
	/*
	 * Ottimizzaione
	 */
	public static Node getLowestCommonAncestor(Node a, Node b) {
		Node ancestor = null;
		
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			RefTypeNode refTypeA = (RefTypeNode) a;
			RefTypeNode refTypeB = (RefTypeNode) b;
			
			return getCommonAncestor(refTypeA.getId(), refTypeB.getId());			
		}
		
		/*
		 * Se uno tra a e b è EmptyTypeNode ritorna l'altro 
		 */
		if (a instanceof EmptyTypeNode) {
			return b;
		}
		
		if (b instanceof EmptyTypeNode) {
			return a;
		} 
		
		/*
		 * Se uno tra a e b è di tipo IntTypeNode ritorna IntTypeNode
		 */
		if (a instanceof IntTypeNode || b instanceof IntTypeNode) {
			return new IntTypeNode();
		}
		
		/*
		 * Se entrambi a e b sono di tipo BoolTypeNode ritorna BoolTypeNode
		 */
		if (a instanceof BoolTypeNode && b instanceof BoolTypeNode) {
			return new BoolTypeNode();
		}
		
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			/*
			 * Accorgimenti
			 * Se entrambi i nodi sono di tipo funzionale ed hanno lo stesso
			 * numero di parametri controllo
			 * - se esiste un lowest common ancestor del tipo di ritorno
			 * - se i tipi dei parametri dell'uno sono sottotipi dei parametri dell'altro
			 * in modo ordinato
			 */
			ArrowTypeNode arrowA = (ArrowTypeNode) a;
			ArrowTypeNode arrowB = (ArrowTypeNode) b;
			
			if (arrowA.getParList().size() == arrowB.getParList().size()) {
				Node commonAncestor = getLowestCommonAncestor(arrowA.getReturnType(), arrowB.getReturnType());
				List<Node> controvariantTypes = getControvariantTypes(arrowA.getParList(), arrowB.getParList());
				if (commonAncestor != null
						&& controvariantTypes.size() == arrowA.getParList().size()) {
					ancestor = new ArrowTypeNode(controvariantTypes, commonAncestor);
				}
			}
		}
		
		return ancestor;			
	}
	
	private static List<Node> getControvariantTypes(List<Node> a, List<Node> b) {
		List<Node> subTypes = new ArrayList<>();

			for (int i = 0; i < a.size(); i++) {
				if (isSubtype(a.get(i), b.get(i))) {
					subTypes.add(a.get(i));
				} else if (isSubtype(b.get(i), a.get(i))){
					subTypes.add(b.get(i));
				}
			}

		return subTypes;
	}
		
	/*	Ottimizzazione pag 64
	 * 	Verifichiamo che b sia sottotipo di a o degli antenati di a.
	 *  Se lo è restituiamo il nodo padre di tipo RefTypeNode
	 */
	public static RefTypeNode getCommonAncestor(String a, String b) {
		
		if (isSubtype(new RefTypeNode(b), new RefTypeNode(a))) {
			return new RefTypeNode(a);
		}
		
		String ancestor = superType.get(a);
		if (superType.get(a) != null) {
			return getCommonAncestor(ancestor, b);				
		}
		return null;		
	}

	/*
	 * Verifichiamo che un tipo EmptyTypeNode sia sottotipo di un qualsiasi
	 * tipo riferimento RefTypeNode
	 */
	private static boolean isEmptyNodeSubtypeOfRefTypeNode(Node a, Node b) {
		return a instanceof EmptyTypeNode && b instanceof RefTypeNode;
	}

	/*
	 * Verifichiamo che un tipo di riferimento RefTypeNode
	 * sia sottotipo di un altro, in base ai rapporti "figlio-padre" della mappa superType 
	 */
	private static boolean isSuperType(Node a, Node b) {
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			RefTypeNode refTypeA = (RefTypeNode) a;
			RefTypeNode refTypeB = (RefTypeNode) b;
			
			String idFather = refTypeA.getId();
			String idChild = refTypeB.getId();
			
			return isSuperType(idFather, idChild);
		} else {
			return false;
		}
	}

	private static boolean isSuperType(String idSuper, String id) {		
		if (superType.containsKey(id)) {
			
			String idChild = superType.get(id);
			String idSuperFather = superType.get(idSuper);
			
			return idSuperFather == id ? true : isSuperType(idSuperFather, idChild);
		}
		return false;
	}
	
	/*
	 * Entrambi i nodi devono essere ArrowTypeNode
	 * e rispettare la covarianza e controvarianza
	 */
	private static boolean isArrowTypeNode(Node a, Node b) {
		return a.getClass().equals(ArrowTypeNode.class)
				&& (b.getClass().equals(ArrowTypeNode.class))
				&& isReturnTypeCovarianceSatisfied(a, b)
				&& isParametersControvarianceSatisfied(a, b);
	}

	/*
	 * Perchè la covarianza sia soddisfatta
	 * il tipo di ritorno del nodo padre deve essere dello stesso tipo
	 * o sottotipo di quello del nodo figlio
	 */
	private static boolean isReturnTypeCovarianceSatisfied(Node a, Node b) {
		Node retA = ((ArrowTypeNode) a).getReturnType();
		Node retB = ((ArrowTypeNode) b).getReturnType();
		
		return isSuperType(retA, retB);
	}

	/*
	 * Perchè la controvarianza sia soddisfatta
	 * - i due nodi devono avere lo stesso numero di parametri
	 * - ogni parametro del nodo figlio, in modo ordinato, deve essere
	 *   dello stesso tipo o supertipo del parametro corrispondente nel nodo padre
	 */
	private static boolean isParametersControvarianceSatisfied(Node a, Node b) {
		ArrowTypeNode aNode = (ArrowTypeNode) a;
		ArrowTypeNode bNode = (ArrowTypeNode) b;

		if (aNode.getParList().size() != bNode.getParList().size()) {
			return false;
		}

		for (int i = 0; i <= aNode.getParList().size(); i++) {
			if (!isSuperType(bNode.getParList().get(i), aNode.getParList().get(i))) {
				return false;
			}
		}
		return true;
	}

}
